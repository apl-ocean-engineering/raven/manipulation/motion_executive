import rospy
from std_msgs.msg import Header
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint

from motion_executive.mover import Mover


def create_joint_trajectory(joint_names, positions, velocities, time_from_start):
    # Initialize the JointTrajectory message
    trajectory = JointTrajectory()
    trajectory.header = Header(frame_id="bravo_base_link")

    # Set the joint names
    trajectory.joint_names = joint_names

    # Create a single JointTrajectoryPoint
    point = JointTrajectoryPoint()

    point.velocities = velocities
    point.positions = positions
    point.time_from_start = time_from_start

    # Add this point to the trajectory
    trajectory.points.append(point)

    return trajectory


if __name__ == "__main__":
    # Initialize the ROS node
    rospy.init_node("joint_trajectory_publisher_node")

    mover = Mover()

    # Define the joint names
    joint_names = [
        "bravo_axis_g",
        "bravo_axis_f",
        "bravo_axis_e",
        "bravo_axis_d",
        "bravo_axis_c",
        "bravo_axis_b",
    ]

    # Initialize the velocities, setting all to 0 except for 'bravo_axis_g'
    velocities = [0.0, 0.0, 0.0, 0.0, 0.0, 0.5]

    # Create the JointTrajectory message

    # Create a publisher to publish the JointTrajectory message
    pub = rospy.Publisher(
        "/arm_velocity_controller/command", JointTrajectory, queue_size=10
    )

    # Set the publishing rate (e.g., 10 Hz)
    rate = rospy.Rate(10)  # 10 Hz

    cnt = 0
    # Loop to continuously publish the message
    while not rospy.is_shutdown():
        positions = mover.arm_move_group_cmdr.get_current_state().joint_state.position[
            0:6
        ]
        if cnt == 0:
            trajectory_msg = create_joint_trajectory(
                joint_names, positions, velocities, rospy.Duration(1)
            )
        else:
            trajectory_msg = create_joint_trajectory(
                joint_names, positions, velocities, rospy.Duration(nsecs=1)
            )

        rospy.loginfo(trajectory_msg)
        pub.publish(trajectory_msg)
        # Sleep to maintain the loop rate
        rate.sleep()

    rospy.loginfo("JointTrajectory message published successfully")
