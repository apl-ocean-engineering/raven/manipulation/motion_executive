#! /usr/bin/env python3

"""
Control Bravo Arm via actions

Author(s):
Marc Micatka, micat001@uw.edu
University of Washington-APL 2024
"""

from typing import Optional, Tuple

import actionlib
import numpy as np
import rospy
import tf2_ros
from dynamic_reconfigure.server import Server
from geometry_msgs.msg import Pose, PoseArray
from nav_msgs.msg import Path
from std_msgs.msg import ColorRGBA, Float32, Header
from tf2_geometry_msgs import PoseStamped, do_transform_pose
from visualization_msgs.msg import MarkerArray

import raven_manip_msgs.msg
from apl_msgs.msg import StringStamped
from motion_executive.cfg import ServoParamsConfig
from motion_executive.conversions import matrix_from_posestamped, path_from_trajectory
from motion_executive.moveit_utils import (
    angle_axis_numpy,
    calculate_distance,
    filter_pose_for_ik_feas,
)
from motion_executive.mover import Mover
from motion_executive.servo import servo_method_dict
from motion_executive.viz_utils import (
    calculate_manipulability_ellipsoid,
    get_ellisoid_marker_array,
)


class MotionServer:
    """
    Node to move bravo arm to positions from action server
    """

    def __init__(self):
        # Member vars:
        self._buffer = tf2_ros.Buffer()
        self._listener = tf2_ros.TransformListener(self._buffer)
        self._planned_trajectory = None
        pid_dict = {}

        try:
            self.verbose = rospy.get_param("~verbose", True)
            self.base_frame = rospy.get_param("~base_frame", "bravo_base_link")
            # max time to allow for individual actions before failing
            self.max_execution_time = rospy.get_param("~max_execution_time", 20)
            servo_method_name = rospy.get_param("~servo_method")
            # PID and Tau values for servoing
            self.xyz_terms = {
                "kp": rospy.get_param("~kp_xyz"),
                "ki": rospy.get_param("~ki_xyz"),
                "kd": rospy.get_param("~kd_xyz"),
                "tau": rospy.get_param("~tau_xyz"),
            }
            self.rpy_terms = {
                "kp": rospy.get_param("~kp_rpy"),
                "ki": rospy.get_param("~ki_rpy"),
                "kd": rospy.get_param("~kd_rpy"),
                "tau": rospy.get_param("~tau_rpy"),
            }
            self.goal_position_tolerance = rospy.get_param("~goal_position_tolerance")
            self.goal_orientation_tolerance = rospy.get_param(
                "~goal_orientation_tolerance"
            )

            pid_dict["xyz_terms"] = self.xyz_terms
            pid_dict["rpy_terms"] = self.rpy_terms
            pid_dict["goal_position_tolerance"] = self.goal_position_tolerance
            pid_dict["goal_orientation_tolerance"] = self.goal_orientation_tolerance

        except Exception as ex:
            rospy.logerr(f"{ex} - Could not get params!")
            return

        if servo_method_name in servo_method_dict.keys():
            rospy.loginfo(f"Using Servo Method: {servo_method_name}")
            servo_method = servo_method_dict[servo_method_name]
        else:
            rospy.logerr(f"No Servo Method: {servo_method_name}!")

        # feedback publishing rate for servoing
        self.rate = rospy.Rate(20)

        # Instantiate Mover:
        self.mover = Mover()
        self.planning_frame = self.mover.arm_move_group_cmdr.get_planning_frame()
        rospy.loginfo(
            f"Planning frame for {self.mover.arm_move_group_cmdr.get_name()}: {self.planning_frame}"
        )
        self._cached_acm = self.mover.scene.get_planning_scene(
            components=128
        ).allowed_collision_matrix  # 128 is the code for ACM

        self.ee_frame_id = "ee_link"  # name of end effector frame

        # Setup PID controller, LPF, and Construct the Servoer
        self.servoer = servo_method(pid_params=pid_dict, ee_frame_id=self.ee_frame_id)

        # Publishers
        self.logging_publisher = rospy.Publisher(
            "~logger", StringStamped, queue_size=10
        )
        self.path_publisher = rospy.Publisher("~planned_path", Path, queue_size=10)
        self.posearray_pub = rospy.Publisher(
            "~planned_path_poses", PoseArray, queue_size=10
        )
        self.offset_goal_pose_pub = rospy.Publisher(
            "~offset_goal_pose", PoseStamped, queue_size=10
        )

        # Ellisoids showing Manipulability:
        self.xyz_ellipsoid_pub = rospy.Publisher(
            "~xyz_ellipsoid", MarkerArray, queue_size=10
        )
        self.rpy_ellipsoid_pub = rospy.Publisher(
            "~rpy_ellipsoid", MarkerArray, queue_size=10
        )

        self.manipulability_metric = rospy.Publisher(
            "~manipulability", Float32, queue_size=10
        )

        self.goal_pose_pub = rospy.Publisher(
            "goal_pose", data_class=PoseStamped, queue_size=10
        )

        # Start the IK service for move to pose
        rospy.wait_for_service("compute_ik")

        # Start actionlib_server for grasp planning:
        self._plan_grasp_server = actionlib.SimpleActionServer(
            "~plan_grasp",
            raven_manip_msgs.msg.PlanToGraspAction,
            execute_cb=self.plan_grasp_callback,
            auto_start=False,
        )

        # Start actionlib_server for single joint motion:
        self._move_joint_server = actionlib.SimpleActionServer(
            "~move_joint",
            raven_manip_msgs.msg.MoveJointAction,
            execute_cb=self.move_joint_callback,
            auto_start=False,
        )

        # Start actionlib_server for moving to a pose:
        self._move_to_pose_server = actionlib.SimpleActionServer(
            "~move_to_pose",
            raven_manip_msgs.msg.MoveToPoseAction,
            execute_cb=self.move_to_pose_callback,
            auto_start=False,
        )

        self._servo_to_pose_server = actionlib.SimpleActionServer(
            "~servo_to_pose",
            raven_manip_msgs.msg.ServoToPoseAction,
            execute_cb=self.servo_to_pose_callback,
            auto_start=False,
        )

        # Start the actionlib servers
        self._plan_grasp_server.start()
        self._move_joint_server.start()
        self._move_to_pose_server.start()
        self._servo_to_pose_server.start()

        # Setup reconfigure server
        self.dyn_reconfigure_srv = Server(ServoParamsConfig, self.reconfigure_callback)

    def reconfigure_callback(self, config: dict, level: int):
        """
        Reconfigures the PID and tau values used in servoing

        Args:
            config (dict): dictionary with config values
            level (int): ? unused.

        Returns:
            config: updated config
        """
        xyz_terms = {
            "kp": config["kp_xyz"],
            "ki": config["ki_xyz"],
            "kd": config["kd_xyz"],
            "tau": config["tau_xyz"],
        }
        rpy_terms = {
            "kp": config["kp_rpy"],
            "ki": config["ki_rpy"],
            "kd": config["kd_rpy"],
            "tau": config["tau_rpy"],
        }

        for key in set(xyz_terms.keys()) | set(self.xyz_terms.keys()):
            if key in xyz_terms and key in self.xyz_terms:
                if xyz_terms[key] != self.xyz_terms[key]:
                    rospy.loginfo(
                        f"xyz key '{key}' changed from {self.xyz_terms[key]} to {xyz_terms[key]}"
                    )
                    self.xyz_terms[key] = xyz_terms[key]

        for key in set(rpy_terms.keys()) | set(self.rpy_terms.keys()):
            if key in rpy_terms and key in self.rpy_terms:
                if rpy_terms[key] != self.rpy_terms[key]:
                    rospy.loginfo(
                        f"rpy key '{key}' changed from {self.rpy_terms[key]} to {rpy_terms[key]}"
                    )
                    self.rpy_terms[key] = rpy_terms[key]

        # # Update Controller
        kp = (self.xyz_terms["kp"], self.rpy_terms["kp"])
        ki = (self.xyz_terms["ki"], self.rpy_terms["ki"])
        kd = (self.xyz_terms["kd"], self.rpy_terms["kd"])
        self.ee_pid = self.servoer.ee_pid.set_params(kp, ki, kd)

        self.servoer.goal_pose_filter.set_tau(
            (self.xyz_terms["tau"], self.rpy_terms["tau"])
        )

        if config["goal_position_tolerance"] != self.goal_position_tolerance:
            rospy.loginfo(
                f"goal orientation tolerance changed from {self.goal_orientation_tolerance} to {config['goal_position_tolerance']}"
            )
            self.goal_orientation_tolerance = config["goal_position_tolerance"]

        if config["goal_position_tolerance"] != self.goal_position_tolerance:
            rospy.loginfo(
                f"goal position tolerance changed from {self.goal_position_tolerance} to {config['goal_position_tolerance']}"
            )
            self.goal_orientation_tolerance = config["goal_position_tolerance"]

        return config

    def plan_grasp_callback(self, goal: raven_manip_msgs.msg.PlanToGraspGoal):
        """
        Callback for the PlanToGrasp action.

        Args:
            goal (PlanToGraspGoal): The action goal containing the input grasp
        """
        rospy.loginfo("Received PlanToGraspAction...")
        result = raven_manip_msgs.msg.PlanToGraspResult()

        grasp = goal.grasp
        offset_pose = PoseStamped()
        offset_pose.header = grasp.header
        offset_pose.pose = grasp.offset_pose
        success = False

        try:
            success = filter_pose_for_ik_feas(
                offset_pose, self.mover.arm_move_group_cmdr
            )

            if not success:
                rospy.logerr("Offset Goal is not IK Feasible!")
            goal_pose = PoseStamped()
            goal_pose.header = grasp.header
            goal_pose.pose = grasp.pose
            success = filter_pose_for_ik_feas(goal_pose, self.mover.arm_move_group_cmdr)

            if not success:
                rospy.logerr("Grasp Goal is not IK Feasible!")

        except Exception as ex:
            rospy.logerr(f"Couldn't filter for IK feasibility: {ex}")

        if len(goal.links) > 0:
            success = self.update_acm(goal.links, goal.allow_collisions)
            if not success:
                self._plan_grasp_server.set_aborted(text="Failed to update ACM!")
        try:
            success, self._planned_trajectory = self.plan_to_pose(offset_pose)

        except Exception as ex:
            rospy.logerr(f"Failed to plan to grasp: {ex}")
            self._plan_grasp_server.set_aborted(text="Failed to plan to grasp!")

        if success:
            try:
                path = path_from_trajectory(
                    self._planned_trajectory.joint_trajectory,
                    self.mover.arm_move_group_cmdr,
                )
                result.path = path
                result.trajectory = self._planned_trajectory
                self._plan_grasp_server.set_succeeded(result)
                rospy.loginfo("Planned to offset_pose!")
            except Exception as ex:
                rospy.logerr(f"Failed to publish trajectory: {ex}")
                self._plan_grasp_server.set_aborted(
                    text="Failed to publish trajectory!"
                )
        else:
            self._plan_grasp_server.set_aborted(text="Failed to plan to grasp!")

    def move_joint_callback(self, goal: raven_manip_msgs.msg.MoveJointGoal):
        """
        Callback for the MoveJoint action.

        Args:
            goal (MoveJointGoal): The action goal
        """
        self.log_data("Received MoveJointAction...")
        feedback = raven_manip_msgs.msg.MoveJointFeedback()
        move_result = raven_manip_msgs.msg.MoveJointResult()

        # Move joint to position
        result = self.mover.move_joint(goal.joint, goal.desired_position, wait=False)

        if result is False:
            move_result.state.state = raven_manip_msgs.msg.TaskState.MOTION_FAILED

            self._move_joint_server.set_aborted(
                move_result, text="Can't move like that"
            )
            self.log_data("MoveJointAction failed")
            return

        # Set start time for timeout
        start_time = rospy.Time.now()

        while not rospy.is_shutdown():
            # Provide feedback
            current_position = self.mover.get_joint_value(goal.joint)

            # Check for success/publish feedback
            if (
                np.abs(current_position - goal.desired_position)
                >= self.goal_position_tolerance
            ):
                feedback.state.state = raven_manip_msgs.msg.TaskState.SERVOING
                feedback.current_position = current_position
                self._move_joint_server.publish_feedback(feedback)

            else:
                # Send result
                move_result.state.state = (
                    raven_manip_msgs.msg.TaskState.MOTION_SUCCEEDED
                )
                self._move_joint_server.set_succeeded(move_result)
                self.log_data("MoveJointAction Completed")
                return

            # Check for a timeout
            if rospy.Time.now() - start_time > rospy.Duration(self.max_execution_time):
                self.log_data("MoveJointAction timed out. Aborting.")
                self.mover.arm_move_group_cmdr.stop()
                self.mover.hand_move_group_cmdr.stop()

                move_result.state.state = raven_manip_msgs.msg.TaskState.MOTION_FAILED
                self._move_joint_server.set_aborted(move_result, text="Task timed out")
                return

            # Check for a preempt
            if self._move_joint_server.is_preempt_requested():
                self.log_data("MoveJointAction Preempt requested. Stopping the motion.")
                self.mover.arm_move_group_cmdr.stop()
                self.mover.hand_move_group_cmdr.stop()
                self._move_joint_server.set_preempted(
                    text="MoveJointAction preempted. Stopping"
                )
                self.log_data("MoveJointAction Preempted")
                return

    def move_to_pose_callback(self, goal: raven_manip_msgs.msg.MoveToPoseAction):
        """
        Callback for the MoveToPose Action.

        Args:
            goal (MoveToPoseGoal): The action goal
        """
        str_data = "Received MoveToPoseAction"
        self.log_data(str_data)

        feedback = raven_manip_msgs.msg.MoveToPoseFeedback()
        move_result = raven_manip_msgs.msg.MoveToPoseResult()

        # Get goal information:
        try:
            success, planned_trajectory = self.plan_to_pose(goal.pose)

            if not success:
                move_result.state.state = raven_manip_msgs.msg.TaskState.MOTION_FAILED
                self._move_to_pose_server.set_aborted(
                    move_result, text="MoveToPoseAction Failed to Plan to Pose"
                )
                return

        except Exception as ex:
            rospy.logerr(f"MoveToPose ActionFailed to plan to pose: {ex}")
            move_result.state.state = raven_manip_msgs.msg.TaskState.MOTION_FAILED
            self._move_to_pose_server.set_aborted(
                move_result, text="MoveToPose Failed to Plan to Pose"
            )
            return

        target_time = self.get_trajectory_execution_time(planned_trajectory)

        rospy.loginfo(
            f"MoveToPose estimating: {target_time:.2f}s for trajectory completion"
        )

        # Move goal pose to the planning frame
        goal_pose = goal.pose
        if goal.pose.header.frame_id != self.planning_frame:
            try:
                transform = self._buffer.lookup_transform(
                    self.planning_frame,
                    goal.pose.header.frame_id,
                    goal.pose.header.stamp,
                    rospy.Duration(1.0),
                )

            except (
                tf2_ros.LookupException,
                tf2_ros.ConnectivityException,
                tf2_ros.ExtrapolationException,
            ) as ex:
                rospy.logerr(f"Transform failed: {ex}")
                return

            # transform the pose
            goal_pose = do_transform_pose(goal.pose, transform)

        # start the move, non-blocking
        self.log_data("starting move to pose", print_info=False)
        # self.mover.move_ee_to_pose(goal_pose, wait=True)
        # self.mover.arm_move_group_cmdr.execute(planned_trajectory, wait=False)
        self.mover.arm_move_group_cmdr.set_pose_target(
            goal_pose, end_effector_link=self.ee_frame_id
        )

        self.mover.arm_move_group_cmdr.go(wait=True)

        t0 = rospy.Time.now()
        while not rospy.is_shutdown():
            within_distance_thresh, time_exceeded = self.move_is_complete(
                goal_pose, target_time, t0
            )

            # Check for success/publish feedback
            if not within_distance_thresh:
                feedback.state.state = raven_manip_msgs.msg.TaskState.MOVING_TO_PLAN
                feedback.current_pose = self.mover.get_ee_pose()
                self._move_to_pose_server.publish_feedback(feedback)

            elif within_distance_thresh:
                # Send result
                move_result.state.state = (
                    raven_manip_msgs.msg.TaskState.MOTION_SUCCEEDED
                )
                self._move_to_pose_server.set_succeeded(move_result)
                str_data = (
                    f"MoveToPose Completed in {(rospy.Time.now() - t0).to_sec():.2f}s"
                )
                self.log_data(str_data)
                return

            if time_exceeded:
                str_data = f"MoveToPose timed out. Aborting after {(rospy.Time.now() - t0).to_sec():.2f}s"
                self.log_data(str_data)

                self.mover.arm_move_group_cmdr.stop()
                self.mover.hand_move_group_cmdr.stop()

                move_result.state.state = raven_manip_msgs.msg.TaskState.MOTION_FAILED
                self._move_to_pose_server.set_aborted(
                    move_result, text="MoveToPose timed out"
                )
                return

            # Check for a preempt
            if self._move_to_pose_server.is_preempt_requested():
                rospy.loginfo("MoveToPose Preempt requested. Stopping the motion.")
                self.mover.arm_move_group_cmdr.stop()
                self.mover.hand_move_group_cmdr.stop()
                self._move_to_pose_server.set_preempted(
                    text="MoveToPose Motion preempted. Stopping"
                )
                rospy.loginfo("MoveToPose Preempted")
                return

    def servo_to_pose_callback(self, goal: raven_manip_msgs.msg.ServoToPoseAction):
        """
        Callback for the ServoToPose Action.

        Args:
            goal (ServoToPoseGoal): The action goal
        """
        rospy.loginfo("Received ServoToPoseAction...")
        feedback = raven_manip_msgs.msg.ServoToPoseFeedback()
        move_result = raven_manip_msgs.msg.ServoToPoseResult()

        # Move goal pose to the planning frame
        if goal.pose.header.frame_id != self.planning_frame:
            try:
                transform = self._buffer.lookup_transform(
                    self.planning_frame,
                    goal.pose.header.frame_id,
                    goal.pose.header.stamp,
                    rospy.Duration(1.0),
                )
                goal_pose = do_transform_pose(goal.pose, transform)

            except (
                tf2_ros.LookupException,
                tf2_ros.ConnectivityException,
                tf2_ros.ExtrapolationException,
            ) as ex:
                rospy.logerr(f"Transform failed: {ex}")
                return
        else:
            goal_pose = goal.pose

        # transform the current pose to planning frame:
        try:
            current_pose = self.mover.get_ee_pose()
            transform = self._buffer.lookup_transform(
                self.planning_frame,
                current_pose.header.frame_id,
                current_pose.header.stamp,
                rospy.Duration(1.0),
            )

            current_pose = do_transform_pose(current_pose, transform)

        except (
            tf2_ros.LookupException,
            tf2_ros.ConnectivityException,
            tf2_ros.ExtrapolationException,
        ) as ex:
            rospy.logerr(f"Transform failed: {ex}")
            return

        # Start Servoing
        start_time = rospy.Time.now()
        self.servoer.goal_pose_filter.update(current_pose)

        while not rospy.is_shutdown():
            # Publish the filtered goal pose for introspection
            self.servoer.update_pose(goal_pose)
            if self.servoer.goal_pose is None:
                self.rate.sleep()
            else:
                self.servoer.step()

            # Check for success/publish feedback
            within_distance_thresh, time_exceeded = self.move_is_complete(
                goal_pose, 20, start_time
            )

            if not within_distance_thresh:
                feedback.state.state = raven_manip_msgs.msg.TaskState.MOVING_TO_PLAN
                feedback.current_pose = self.mover.get_ee_pose()
                self._servo_to_pose_server.publish_feedback(feedback)
                self.rate.sleep()

            elif within_distance_thresh and not time_exceeded:
                # Send result
                move_result.state.state = (
                    raven_manip_msgs.msg.TaskState.MOTION_SUCCEEDED
                )
                self._servo_to_pose_server.set_succeeded(move_result)
                rospy.loginfo(
                    f"ServoToPoseAction Completed in {((rospy.Time.now() - start_time).to_sec()):.2f}s"
                )
                self.mover.arm_move_group_cmdr.stop()
                self.mover.hand_move_group_cmdr.stop()

                self.servoer.publish_halt()
                return

            if time_exceeded:
                rospy.loginfo(
                    f"ServoToPoseAction timed out. Aborting after {(rospy.Time.now() - start_time).to_sec():.2f}s"
                )
                self.mover.arm_move_group_cmdr.stop()
                self.mover.hand_move_group_cmdr.stop()

                # Publish zero twist to stop motion
                self.servoer.publish_halt()

                move_result.state.state = raven_manip_msgs.msg.TaskState.MOTION_FAILED
                self._servo_to_pose_server.set_aborted(
                    move_result, text="ServoToPose timed out"
                )
                return

            # Check for a preempt
            if self._servo_to_pose_server.is_preempt_requested():
                rospy.loginfo(
                    "ServoToPoseAction Preempt requested. Stopping the motion."
                )

                # Publish zero twist to stop motion
                self.servoer.publish_halt()

                self.mover.arm_move_group_cmdr.stop()
                self.mover.hand_move_group_cmdr.stop()
                # Send result
                move_result.state.state = raven_manip_msgs.msg.TaskState.MOTION_FAILED
                self._servo_to_pose_server.set_preempted(
                    move_result, text="ServoToPoseAction Motion preempted. Stopping"
                )
                rospy.loginfo("ServoToPoseAction Preempted")
                return

    def move_is_complete(
        self, goal_pose: PoseStamped, target_time: float, start_time: float
    ) -> Tuple:
        """
        Checks whether a move has completed.

        Args:
            goal_pose (PoseStamped): target pose for EE
            target_time (float): Estimated trajectory completion time
            start_time (float): Start time
        Returns:
            Tuple[bool, bool]: distance completed? Time completed?
        """
        # Check the distance to target
        position_error = np.abs(
            calculate_distance(self.mover.get_ee_pose().pose, goal_pose)
        )
        # Calculate angle-axis error:
        orientation_error = angle_axis_numpy(
            matrix_from_posestamped(self.mover.get_ee_pose()),
            matrix_from_posestamped(goal_pose),
        )
        within_threshold = position_error <= self.goal_position_tolerance and np.all(
            orientation_error
        )

        # Check if we've exceeded the target time
        target_time_exceeded = (rospy.Time.now() - start_time) >= rospy.Duration(
            5 * target_time
        )

        # Check if we've exceeded the max time
        max_time_exceeded = rospy.Time.now() - start_time > rospy.Duration(
            self.max_execution_time
        )
        time_check = target_time_exceeded or max_time_exceeded
        return within_threshold, time_check

    def plan_to_pose(self, pose: PoseStamped) -> Tuple:
        """
        Plan to a pose.

        Args:
            pose (PoseStamped): pose to plan to.

        Returns:
            bool: planning success
        """
        if self.verbose:
            if type(pose) is PoseStamped:
                rospy.loginfo(
                    f"Planning to move to pose: xyz: [{pose.pose.position.x:.2f}, {pose.pose.position.y:.2f}, {pose.pose.position.z:.2f}]"
                )

                self.offset_goal_pose_pub.publish(pose)
            elif type(pose) is Pose:
                rospy.loginfo(
                    f"Planning to move to pose: xyz: [{pose.position.x:.2f}, {pose.position.y:.2f}, {pose.position.z:.2f}]"
                )

        self.mover.arm_move_group_cmdr.set_pose_target(
            pose, end_effector_link=self.ee_frame_id
        )

        (
            success,
            robot_traj,
            _,
            _,
        ) = self.mover.arm_move_group_cmdr.plan()

        return success, robot_traj

    def transform_grasp_msg(
        self, grasp_msg: raven_manip_msgs.msg.Grasp, target_frame: str
    ) -> raven_manip_msgs.msg.Grasp:
        """
        Transform incoming grasp message to the arm's base frame

        Args:
            grasp_msg (Grasp): non-transformed grasp message

        Returns:
            Grasp: Transformed grasp message
        """

        tf_grasp_msg = grasp_msg
        tf_grasp_msg.header.frame_id = target_frame

        try:
            transform = self._buffer.lookup_transform(
                target_frame,
                grasp_msg.header.frame_id,
                grasp_msg.header.stamp,
                rospy.Duration(1.0),
            )
        except (
            tf2_ros.LookupException,
            tf2_ros.ConnectivityException,
            tf2_ros.ExtrapolationException,
        ) as ex:
            rospy.logerr(f"Transform failed: {ex}")

        try:
            tmp_ps = PoseStamped(header=grasp_msg.header, pose=grasp_msg.pose)
            tf_ps = do_transform_pose(tmp_ps, transform)
            tmp_offset_ps = PoseStamped(
                header=grasp_msg.header, pose=grasp_msg.offset_pose
            )
            tf_offset_ps = do_transform_pose(tmp_offset_ps, transform)

            tf_grasp_msg.pose = tf_ps.pose
            tf_grasp_msg.offset_pose = tf_offset_ps

            if self.verbose:
                rospy.loginfo(
                    f"Transformed goal from {grasp_msg.header.frame_id} to {target_frame}"
                )
        except Exception as ex:
            rospy.logwarn(f"Failed to transform pose to {target_frame} - {ex}")

        return tf_grasp_msg

    def get_trajectory_execution_time(self, trajectory):
        # Check if trajectory is empty
        if not trajectory.joint_trajectory.points:
            rospy.logwarn("The trajectory is empty!")
            return 0.0

        # Get the last trajectory point and its time stamp
        # Trajectories are often timing out, use a hefty time safety factor of 2
        last_point = trajectory.joint_trajectory.points[-1]
        execution_time = 2 * last_point.time_from_start.to_sec()

        return execution_time

    def run(self):
        while not rospy.is_shutdown():
            jacobian = self.mover.arm_move_group_cmdr.get_jacobian_matrix(
                self.mover.arm_move_group_cmdr.get_current_joint_values()
            )

            # Publish this value
            manipulability = np.linalg.det(jacobian @ jacobian.T) ** 0.5
            self.manipulability_metric.publish(Float32(data=manipulability))

            (
                (pos_eigenvalues, pos_eigenvectors),
                (
                    ori_eigenvalues,
                    ori_eigenvectors,
                ),
            ) = calculate_manipulability_ellipsoid(jacobian)

            position_marker = get_ellisoid_marker_array(
                pos_eigenvalues,
                pos_eigenvectors,
                0,
                ColorRGBA(1.0, 0.0, 0.0, 0.5),
                frame_id="ee_link",
                scale=0.5,
            )
            self.xyz_ellipsoid_pub.publish(position_marker)

            orientation_marker = get_ellisoid_marker_array(
                ori_eigenvalues,
                ori_eigenvectors,
                2,
                ColorRGBA(0.0, 0.0, 1.0, 0.5),
                frame_id="ee_link",
                scale=0.1,
            )
            self.rpy_ellipsoid_pub.publish(orientation_marker)

            self.rate.sleep()

    def log_data(self, str_data: str, print_info=True) -> None:
        """
        Publish string stamped data

        Args:
            str_data (str): Incoming string data
            print_info (bool): If true, rospy.loginfo the str data
        """
        current_header = Header(stamp=rospy.Time.now())
        self.logging_publisher.publish(
            StringStamped(header=current_header, data=str_data)
        )

        if print_info:
            rospy.loginfo(str_data)

    def update_acm(self, links: Optional[list], allow_collisions: bool) -> bool:
        """
        Update the allowed collision matrix with a list of links and whether to allow collisions or not

        Args:
            links (Optional[list]): Links to change ACM
            allow_collisions (bool): Allow collisions for links

        Returns:
            bool: Success
        """
        # Default links
        links_to_allow = (
            links if links else ["bravo_lower_gripper_link", "bravo_upper_gripper_link"]
        )

        # Turn collisions back to cached state:
        if allow_collisions:
            # Update the AllowedCollisionMatrix
            scene = self.mover.scene.get_planning_scene(components=128)
            acm = scene.allowed_collision_matrix

            for link in links_to_allow:
                rospy.loginfo(f"Turning Collisions off for {link}")
                if link not in acm.entry_names:
                    return False
                index = acm.entry_names.index(link)
                # Turn collisions off == allow collisions
                acm.entry_values[index].enabled = [True] * len(acm.entry_names)

        # reset collisions to cached state
        else:
            acm = self._cached_acm

        # Apply the updated planning scene
        try:
            scene.allowed_collision_matrix = acm
            scene.is_diff = True
            self.mover.scene.apply_planning_scene(scene)
            rospy.loginfo("updated ACM!")
            return True
        except rospy.ServiceException:
            return False


if __name__ == "__main__":
    try:
        rospy.init_node("motion_server")
        motion_server = MotionServer()
        motion_server.run()
    except rospy.ROSInterruptException:
        pass
