"""
ROS Node which launches an 6DOF interactive marker (a la Motion Planning)
in rviz.  Any time the marker is updated, an updated PoseStamped is
published to the topic "marker_pose".

If the param "~initial_tf" is provided, the marker will initialize
with the pose of the specified tf.   The marker will not appear
until it has resolved the tf.

If the param is not specified, the marker initializes at the
PostStamped default (at the origin, zero R,P,Y)

Author(s):
Aaron Marburg <amarburg@uw.edu>
University of Washington-APL 2024
"""

import rospy
import tf2_ros
from geometry_msgs.msg import Pose, PoseStamped, TransformStamped
from interactive_markers.interactive_marker_server import InteractiveMarkerServer
from scipy.spatial.transform import Rotation
from std_msgs.msg import Header
from visualization_msgs.msg import InteractiveMarker, InteractiveMarkerControl


def normalize_quaternion(msg):
    quat = Rotation.from_quat([msg.x, msg.y, msg.z, msg.w]).as_quat()
    msg.x = quat[0]
    msg.y = quat[1]
    msg.z = quat[2]
    msg.w = quat[3]


class ServoingGoalSimulator:
    def __init__(self, global_frame, server_name):
        self.server = InteractiveMarkerServer(server_name)

        self.pose_pub = rospy.Publisher("marker_pose", PoseStamped, queue_size=10)

        self.global_frame = global_frame

    def make_6dof_marker(self, position, quat):
        int_marker = InteractiveMarker()
        int_marker.header.frame_id = self.global_frame
        int_marker.pose.position = position
        int_marker.pose.orientation = quat
        int_marker.scale = 0.25

        int_marker.name = "simple_6dof"
        int_marker.description = "Simple 6-DOF Control"

        control = InteractiveMarkerControl()
        control.orientation.w = 1
        control.orientation.x = 1
        control.orientation.y = 0
        control.orientation.z = 0
        normalize_quaternion(control.orientation)
        control.name = "rotate_x"
        control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS

        int_marker.controls.append(control)

        control = InteractiveMarkerControl()
        control.orientation.w = 1
        control.orientation.x = 1
        control.orientation.y = 0
        control.orientation.z = 0
        normalize_quaternion(control.orientation)
        control.name = "move_x"
        control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS

        int_marker.controls.append(control)

        control = InteractiveMarkerControl()
        control.orientation.w = 1
        control.orientation.x = 0
        control.orientation.y = 1
        control.orientation.z = 0
        normalize_quaternion(control.orientation)
        control.name = "rotate_y"
        control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS

        int_marker.controls.append(control)

        control = InteractiveMarkerControl()
        control.orientation.w = 1
        control.orientation.x = 0
        control.orientation.y = 1
        control.orientation.z = 0
        normalize_quaternion(control.orientation)
        control.name = "move_y"
        control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS

        int_marker.controls.append(control)

        control = InteractiveMarkerControl()
        control.orientation.w = 1
        control.orientation.x = 0
        control.orientation.y = 0
        control.orientation.z = 1
        normalize_quaternion(control.orientation)
        control.name = "rotate_z"
        control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS

        int_marker.controls.append(control)

        control = InteractiveMarkerControl()
        control.orientation.w = 1
        control.orientation.x = 0
        control.orientation.y = 0
        control.orientation.z = 1
        normalize_quaternion(control.orientation)
        control.name = "move_z"
        control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS

        int_marker.controls.append(control)

        self.server.insert(int_marker, self.publish_pose_callback)
        # menu_handler.apply( server, int_marker.name )

    def apply_changes(self):
        self.server.applyChanges()

    def publish_pose_callback(self, feedback):
        # Publish the pose of the charging station
        self.pose = PoseStamped()
        self.pose.header = Header()
        self.pose.header.frame_id = self.global_frame
        self.pose.header.stamp = rospy.Time.now()
        self.pose.pose = Pose()

        self.pose.pose.position = feedback.pose.position
        self.pose.pose.orientation = feedback.pose.orientation

        self.pose_pub.publish(self.pose)


if __name__ == "__main__":
    rospy.init_node("interactive_marker")

    try:
        global_frame = rospy.get_param("~global_frame", "robot_base_link")

        if rospy.has_param("~initial_tf"):
            initial_tf = rospy.get_param("~initial_tf")
            rospy.loginfo(f"Waiting for initial frame {initial_tf}")

            tf_buffer = tf2_ros.Buffer()
            listener = tf2_ros.TransformListener(tf_buffer)
            rate = rospy.Rate(1.0)

            have_transform = False
            while not have_transform:
                try:
                    trans = tf_buffer.lookup_transform(
                        global_frame, initial_tf, rospy.Time()
                    )
                    break
                except (
                    tf2_ros.LookupException,
                    tf2_ros.ConnectivityException,
                    tf2_ros.ExtrapolationException,
                ):
                    rate.sleep()
                    continue

        else:
            # If not specified, start at origin
            trans = TransformStamped()

        server_name = rospy.get_param("~server_name", rospy.get_name())
        rospy.loginfo(f"Starting server {server_name}")
        rospy.loginfo(f"Got initial position: {trans}")

        goal_sim = ServoingGoalSimulator(global_frame, server_name)

        goal_sim.make_6dof_marker(
            trans.transform.translation,
            trans.transform.rotation,
        )
        # goal_sim.create_marker()
        goal_sim.apply_changes()
    except rospy.ROSInterruptException:
        pass

    rospy.spin()
