"""
CLI to control Bravo Arm through series of poses loaded from YAML

Author(s):
Marc Micatka, micat001@uw.edu
University of Washington-APL 2023
"""

import os
import time

import rospkg
import rospy
from geometry_msgs.msg import Point, Pose, PoseArray, Quaternion
from nav_msgs.msg import Path
from prettytable import PrettyTable
from std_msgs.msg import Header
from visualization_msgs.msg import MarkerArray

from apl_msgs.msg import StringStamped
from motion_executive.conversions import (
    get_ros_colors,
    markers_from_pose,
    path_from_trajectory,
    poses_from_yaml,
)
from motion_executive.moveit_utils import filter_for_ik_feas
from motion_executive.mover import Mover

# Initialize the ROS package manager
rospack = rospkg.RosPack()
pkg_root = rospack.get_path("motion_executive")


def publish_status(msg: str, string_pub: rospy.Publisher, verbose=True) -> None:
    """
    Publish a StringStamped message

    Args:
        msg (str): msg to publish
        string_pub (Publisher): rospy Publisher
    """
    status = StringStamped()
    status.header = Header(stamp=rospy.Time.now())
    status.data = msg
    string_pub.publish(status)

    if verbose:
        print(msg)


class TrajectoryPlanner:
    """
    CLI tool to move to pose/named states
    """

    def __init__(self):
        # Instantiate Mover:
        self.mover = Mover()
        self.planning_frame = self.mover.arm_move_group_cmdr.get_planning_frame()
        rospy.loginfo(
            f"Planning frame for {self.mover.arm_move_group_cmdr.get_name()}: {self.planning_frame}"
        )

        # Setup Services for FK/IK
        rospy.wait_for_service("compute_fk")
        rospy.wait_for_service("compute_ik")

        # Get filepath
        yaml_filename = rospy.get_param("~config_file", "cfg/poses.yaml")
        self.yaml_filepath = os.path.join(pkg_root, yaml_filename)

        # Get Named Poses from SRDF for the purposes of plotting the EE pose in RViz:
        named_state_poses = {}
        for name in self.mover.arm_move_group_cmdr.get_named_targets():
            pose_stamped = self.mover.get_pose_of_named_state(name)
            position = pose_stamped.pose.position
            orientation = pose_stamped.pose.orientation
            named_state_poses[name] = [
                position.x,
                position.y,
                position.z,
                orientation.x,
                orientation.y,
                orientation.z,
                orientation.w,
            ]

        # Setup Member Variables
        self.rate = rospy.Rate(30)
        self.defined_poses = None  # These are from the YAML
        self.named_state_poses = named_state_poses  # poses from SRDF named states
        self.ee_frame_id = "ee_link"
        self.marker_colors = None

        # Marker Publications:
        self.path_publisher = rospy.Publisher("~planned_path", Path, queue_size=10)

        self.marker_publisher = rospy.Publisher(
            "~pose_markers", MarkerArray, queue_size=10
        )
        self.pose_publisher = rospy.Publisher("~axes_markers", PoseArray, queue_size=10)

        self.status_publisher = rospy.Publisher(
            "~mover_status", StringStamped, queue_size=10
        )

        # Read YAML, store poses in defined_poses
        self.get_poses_from_yaml()

    def run(self):
        while not rospy.is_shutdown():
            # Text input from user:
            selection = input(
                "Enter name, index (int), 'list, 'exit', or 'help': "
            ).lower()

            if selection == "exit":
                print("Exiting")
                exit()
            elif selection in ["list", "l"]:
                self.print_poses()

            elif selection == "update":
                # update poses from file then return to this state
                self.get_poses_from_yaml()

            elif selection in self.named_state_poses:
                if self.plan_to_named_state(selection):
                    confirmation = input("Move to this pose? (Y/N): ").lower()
                    if confirmation == "exit":
                        print("Exiting")
                        exit()
                    elif confirmation == "y":
                        self.move_to_named_state(selection)
                else:
                    print("Can't find a way there.")

            elif selection.isdigit():
                selection = int(selection)
                if selection in self.defined_poses:
                    if self.plan_to_pose(selection):
                        confirmation = input("Move to this pose? (Y/N): ").lower()
                        if confirmation == "exit":
                            print("Exiting")
                            exit()
                        elif confirmation == "y":
                            self.move_to_pose(selection)
                    else:
                        print("Can't find a way there.")
            else:
                print("Enter name, index (int), 'list, 'exit', or 'help'")

            self.clear_path()

    def print_poses(self):
        """
        Pretty Print the table of poses/named poses
        """

        # Print the list of named states
        table = PrettyTable(["Name"])
        for name in self.named_state_poses.keys():
            table.add_row([name])
        print(table)

        table = PrettyTable(["ID", "XYZ", "Quaternion"])
        for k, v in self.defined_poses.items():
            ps = [round(val, 2) for val in v]
            table.add_row([k, ps[:3], ps[3:]])

        print(table)

    def publish_marker_poses(self):
        """
        Publish marker poses in RViz.
        """
        marker_array = MarkerArray()
        pose_array = PoseArray()

        # Draw pose markers for user-defined poses
        for i, (label, pose_data) in enumerate(self.defined_poses.items()):
            pose = Pose()
            pose.position = Point(*pose_data[:3])
            pose.orientation = Quaternion(*pose_data[3:])

            pose_array.poses.append(pose)
            marker, text_marker = markers_from_pose(
                pose,
                str(label),
                unique_id=i,
                color=self.marker_colors[i],
                frame=self.planning_frame,
            )
            marker_array.markers.append(marker)
            marker_array.markers.append(text_marker)

        # Draw pose markers for named states
        for i, (label, pose_data) in enumerate(self.named_state_poses.items()):
            pose = Pose()
            pose.position = Point(*pose_data[:3])
            pose.orientation = Quaternion(*pose_data[3:])

            pose_array.poses.append(pose)
            marker, text_marker = markers_from_pose(
                pose,
                str(label),
                unique_id=i + 2 * len(marker_array.markers),
                color=self.marker_colors[i],
                frame=self.planning_frame,
            )
            marker_array.markers.append(marker)
            marker_array.markers.append(text_marker)
        if marker_array.markers and pose_array.poses:
            self.marker_publisher.publish(marker_array)
            pose_array.header = marker.header
            self.pose_publisher.publish(pose_array)

    def get_poses_from_yaml(self):
        """
        Update poses.
        * Re-read poses from file
        * Republish markers
        """
        poses = poses_from_yaml(self.yaml_filepath)
        poses = filter_for_ik_feas(poses, self.mover.arm_move_group_cmdr, verbose=True)
        rospy.loginfo(f"Kinematically Feasible User-Defined Poses: {len(poses)}")

        if poses != self.defined_poses:
            self.defined_poses = poses
            self.print_poses()
            self.marker_colors = get_ros_colors(
                num_colors=(len(self.defined_poses) + len(self.named_state_poses))
            )
        else:
            rospy.loginfo("no changes found")
        self.publish_marker_poses()

    def clear_path(self):
        """
        Clear a path by publishing an empty path in RViz
        """
        # Create an empty Path message
        empty_path = Path()
        empty_path.header.frame_id = self.planning_frame

        # Publish the empty path to clear the visualization
        self.path_publisher.publish(empty_path)

    def plan_to_named_state(self, named_state: str) -> bool:
        """
        Plan to a named state

        Args:
            named_state (str): State in SRDF

        Returns:
            bool: planning success
        """
        print(f"Planning to move to state: {named_state}")
        t0 = time.time()
        (
            success,
            robot_traj,
            planning_time,
            error_code,
        ) = self.mover.plan_to_named_state(named_state)
        print(f"Planning took: {1000 * (time.time() - t0):.3f} ms")
        if success:
            path = path_from_trajectory(
                robot_traj.joint_trajectory,
                self.mover.arm_move_group_cmdr,
            )
            self.path_publisher.publish(path)
            return True

        else:
            print("Can't find a way there.")
            return False

    def plan_to_pose(self, pose_idx: int) -> bool:
        """
        Plan to a pose

        Args:
            pose_idx (int): Index into defined poses dictionary

        Returns:
            bool: planning success
        """
        print(f"Planning to move to pose: {self.defined_poses[pose_idx]}")
        self.pose_target = Pose()
        self.pose_target.position = Point(*self.defined_poses[pose_idx][:3])
        self.pose_target.orientation = Quaternion(*self.defined_poses[pose_idx][3:])
        self.mover.arm_move_group_cmdr.set_pose_target(
            self.pose_target, end_effector_link="ee_link"
        )
        t0 = time.time()
        (
            success,
            robot_traj,
            planning_time,
            error_code,
        ) = self.mover.arm_move_group_cmdr.plan()
        print(f"Planning took: {1000 * (time.time() - t0):.3f} ms")
        if success:
            path = path_from_trajectory(
                robot_traj.joint_trajectory,
                self.mover.arm_move_group_cmdr,
            )
            self.path_publisher.publish(path)
            return True

        else:
            print(f"Can't find a way there. {error_code}")
            return False

    def move_to_named_state(self, named_state: str):
        """
        Move to a named state (from SRDF)

        Args:
            named_state (str): State in SRDF
        """
        msg = f"Moving to: {named_state}"
        publish_status(msg, self.status_publisher)

        self.mover.move_to_named_state(named_state)

        msg = f"Finished moving to: {named_state}"
        publish_status(msg, self.status_publisher)

    def move_to_pose(self, pose_idx: int):
        """
        Move to pose (from yaml)


        Args:
            pose_idx (int): Index into defined poses dictionary
        """
        msg = f"Moving to: {self.defined_poses[pose_idx]}, Pose_idx: {pose_idx}"
        publish_status(msg, self.status_publisher)

        # expedient way to get the header/frame
        target_pose = self.mover.get_ee_pose()
        target_pose.pose = self.pose_target
        self.mover.move_ee_to_pose(target_pose)
        msg = (
            f"Finished moving to: {self.defined_poses[pose_idx]}, Pose_idx: {pose_idx}"
        )
        publish_status(msg, self.status_publisher)


if __name__ == "__main__":
    try:
        rospy.init_node("move_to_pose")
        trajectory_planner = TrajectoryPlanner()
        trajectory_planner.run()
    except rospy.ROSInterruptException:
        pass
