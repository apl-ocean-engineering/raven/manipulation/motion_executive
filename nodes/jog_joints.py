import signal
from typing import Dict

import moveit_commander
import PySimpleGUI as sg  # noqa: N813
import rospy
import rosservice

from motion_executive.mover import Mover
from rsa_bravo_msgs.srv import EnableBravo

signal.signal(signal.SIGINT, signal.SIG_DFL)

bravo_joints = [
    "bravo_axis_a",
    "bravo_axis_b",
    "bravo_axis_c",
    "bravo_axis_d",
    "bravo_axis_e",
    "bravo_axis_f",
    "bravo_axis_g",
]


def create_layout() -> list:
    layout = []

    # Header for Bravo status and power switch
    layout.extend(
        [
            [
                sg.Text(
                    "Bravo Disabled",
                    key="bravo_status",
                    size=(15, 1),
                    background_color="red",
                    text_color="white",
                ),
                sg.Button(
                    (
                        "Enable"
                        if "Enable" not in sg.theme_button_color()[0]
                        else "Disable"
                    ),
                    key="enable_button",
                    size=(10, 1),
                ),
            ]
        ]
    )

    # Header for Joint Names, Current/Desired Value, and Jog Joints Buttons
    layout.extend(
        [
            [
                sg.Text("Joints", justification="center"),
                sg.Text(
                    "\tCurrent",
                    justification="center",
                ),
                sg.Text(
                    "\tDesired",
                    justification="center",
                ),
            ],
        ]
    )

    # Create rows for joints A to G
    for joint_label in bravo_joints:
        row = [
            sg.Text(joint_label, size=(15, 1)),
            sg.Text(
                "0.0",
                key=f"{joint_label}_current",
                size=(10, 1),
                justification="center",
            ),
            sg.Text(
                "0.0",
                key=f"{joint_label}_desired",
                size=(10, 1),
                justification="center",
            ),
            sg.Button("+", key=f"{joint_label}_increase"),
            sg.Button("-", key=f"{joint_label}_decrease"),
        ]
        layout.append(row)

    return layout


class JointJogger:
    def __init__(self) -> None:
        # Create service for enabling bravo
        services = rosservice.get_service_list()
        # Check if the "enable_bravo" service is in the list
        enable_service = [s for s in services if "enable_bravo" in s]
        self.enable_bravo_srv = None
        bravo_enable_srv_name = None

        if len(enable_service) > 0:
            bravo_enable_srv_name = enable_service[0]
            rospy.loginfo(f"Service proxy created for {bravo_enable_srv_name}")

        else:
            rospy.logwarn(
                "Service 'enable_bravo' not found in any namespace. Are you in a simulation?"
            )

        if bravo_enable_srv_name is not None:
            rospy.wait_for_service(bravo_enable_srv_name)

        # Create a service proxy for the service
        self.enable_bravo_srv = rospy.ServiceProxy(bravo_enable_srv_name, EnableBravo)

        # False when bravo is disabled, True when bravo is enabled
        self.bravo_enabled_flag = False

        # Mover stuff
        self.mover = Mover()
        self.current_joint_values: Dict[str, float] = {}
        self.desired_joint_values: Dict[str, float] = {}

        # Setup the GUI
        self.setup_layout()

    def set_bravo_state(self, enable: bool) -> bool:
        """
        Enable bravo arm with srv call

        Returns:
            bool: Success
        """
        if self.enable_bravo_srv is None:
            print("Service call failed: proxy not initialized")
            return False

        success = False
        try:
            self.enable_bravo_srv(enable)
            self.bravo_enabled_flag = enable
            success = True
        except rospy.ServiceException as ex:
            print(f"Service call failed: {ex}")
        return success

    def setup_layout(self) -> None:
        """
        Generate PySimpleGUI Layout
        Set current joint states
        """
        sg.theme("Default1")

        layout = create_layout()
        self.window = sg.Window("Bravo Joint Control", layout)

        # read the window so we can update elements by key
        event, values = self.window.read(timeout=20)

        self.current_joint_values = self.mover.get_current_joint_values()
        for k, v in self.current_joint_values.items():
            if k not in bravo_joints:
                continue
            self.window[f"{k}_current"].update(f"{v: .3f}")
            self.window[f"{k}_desired"].update(f"{v: .3f}")

            # Set the desired joint value to the initial value
            self.desired_joint_values[k] = v

        if self.enable_bravo_srv is None:
            # Probably not using the actual arm, so this button does nothing
            self.window["enable_button"].update(disabled=True)
            self.window["bravo_status"].update("N/A", background_color="grey")

    def handle_enable_button(self) -> None:
        """
        Handle the enable_button
        """
        if self.bravo_enabled_flag:
            # Disable Bravo
            success = self.set_bravo_state(False)
            if success:
                self.window["bravo_status"].update(
                    "Bravo Disabled", background_color="red"
                )
                self.window["enable_button"].update("Enable")

        else:
            # Enable Bravo
            success = self.set_bravo_state(True)
            if success:
                self.window["bravo_status"].update(
                    "Bravo Enabled", background_color="green"
                )
                self.window["enable_button"].update("Disable")

    def set_desired_joint_values(self) -> None:
        """
        Update the text with current Desired position
        """

        for joint_label in bravo_joints:
            desired_value = self.desired_joint_values[joint_label]
            self.window[f"{joint_label}_desired"].update(f"{desired_value:.3f}")

    def handle_joint_jog(self, target_joint: str) -> bool:
        """
        Handle a Joint Jog event

        Args:
            target_joint (str): target name of the joint to jog

        Returns:
            success: Whether the jog succeeded or not
        """
        self.current_joint_values = self.mover.get_current_joint_values()

        # Because the gripper is a different move group, needs to be handled separately.
        if target_joint == "bravo_axis_a":
            desired_pos = self.desired_joint_values["bravo_axis_a"]
            return self.mover.move_gripper(desired_pos, wait=False)

        else:
            # Get the current joint values
            joint_values = self.mover.arm_move_group_cmdr.get_current_joint_values()
            joint_names = self.mover.arm_move_group_cmdr.get_active_joints()
            target_joint_index = joint_names.index(target_joint)

            # Update the target joint value in the current_joint_values list
            joint_values[target_joint_index] = self.desired_joint_values[target_joint]

            # Plan and execute the trajectory to the new joint configuration
            try:
                self.mover.arm_move_group_cmdr.set_joint_value_target(joint_values)
                success, _, _, _ = self.mover.arm_move_group_cmdr.plan()
                if success:
                    self.mover.arm_move_group_cmdr.go(wait=False)
                    return True
                else:
                    return False
            except moveit_commander.exception.MoveItCommanderException as ex:
                print(f"Exception: {ex}")
                return False

    def run(self) -> None:
        """
        Main event loop.
        """
        while True:
            event, _ = self.window.read(timeout=20)

            if event == sg.WINDOW_CLOSED:
                break

            # Update arm current positions
            self.current_joint_values = self.mover.get_current_joint_values()
            for k, v in self.current_joint_values.items():
                if k not in bravo_joints:
                    continue
                self.window[f"{k}_current"].update(f"{v:.3f}")

            # Handle power button click
            if event == "enable_button" and self.enable_bravo_srv:
                self.handle_enable_button()

            # Check for button click events
            for joint_label in bravo_joints:
                button_flag = False
                # Check for a button click:
                if event == f"{joint_label}_increase":
                    button_flag = True
                    # Handle increase button click
                    increment = 0.1
                    # move the jaws less than the other joints
                    if joint_label == "bravo_axis_a":
                        increment = 0.005

                # Check for a button click (decrease):
                elif event == f"{joint_label}_decrease":
                    button_flag = True
                    # Handle decrease button click
                    increment = -0.1
                    if joint_label == "bravo_axis_a":
                        increment = -0.005

                if button_flag:
                    self.desired_joint_values[joint_label] += increment
                    self.set_desired_joint_values()
                    success = self.handle_joint_jog(joint_label)

                    if not success:
                        # revert the desired val! We couldnt do it.
                        self.desired_joint_values[joint_label] -= increment
                        self.set_desired_joint_values()

        self.window.close()


if __name__ == "__main__":
    try:
        rospy.init_node("jog_joints")
        jogger = JointJogger()
        jogger.run()
    except rospy.ROSInterruptException:
        pass
