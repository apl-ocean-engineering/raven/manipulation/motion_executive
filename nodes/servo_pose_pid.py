"""
ROS Node for performing end-effector _position_ servoing.
This is largely ROS plumbing which subscribes to an input PoseStamped,
uses an instance of EndEffectorPID to calculate the pose error for
the end effector, and publishes a TwistStamped for use by
MoveIt servo_server (if using the MoveItServo wrapper)

The param "~ee_frame" is the tracked frame.

Author(s):
Aaron Marburg <amarburg@uw.edu>
Marc Micatka
University of Washington-APL 2024
"""

import rospy
from geometry_msgs.msg import PoseStamped

from motion_executive.servo import servo_method_dict


class ServoToPose:
    """
    Use different servo methods to track a PoseStamped msg
    """

    def __init__(self):
        pid_dict = {}
        try:
            self.ee_frame_id = rospy.get_param("~ee_frame")
            servo_method_name = rospy.get_param("~servo_method")

            # PID and Tau values for servoing
            self.xyz_terms = {
                "kp": rospy.get_param("~kp_xyz"),
                "ki": rospy.get_param("~ki_xyz"),
                "kd": rospy.get_param("~kd_xyz"),
                "tau": rospy.get_param("~tau_xyz"),
            }
            self.rpy_terms = {
                "kp": rospy.get_param("~kp_rpy"),
                "ki": rospy.get_param("~ki_rpy"),
                "kd": rospy.get_param("~kd_rpy"),
                "tau": rospy.get_param("~tau_rpy"),
            }
            self.goal_position_tolerance = rospy.get_param("~goal_position_tolerance")

            self.goal_orientation_tolerance = rospy.get_param(
                "~goal_orientation_tolerance"
            )

            pid_dict["xyz_terms"] = self.xyz_terms
            pid_dict["rpy_terms"] = self.rpy_terms
            pid_dict["goal_position_tolerance"] = self.goal_position_tolerance
            pid_dict["goal_orientation_tolerance"] = self.goal_orientation_tolerance

        except Exception as ex:
            rospy.logfatal(f"Could not get params! {ex}")
            return

        if servo_method_name in servo_method_dict.keys():
            rospy.loginfo(f"Using Servo Method: {servo_method_name}")
            servo_method = servo_method_dict[servo_method_name]
        else:
            rospy.logerr(f"No Servo Method: {servo_method_name}!")

        # Construct the Servoer
        self.servoer = servo_method(pid_params=pid_dict, ee_frame_id=self.ee_frame_id)

        # Setup Subscribers
        # Input topic of goal pose
        self.pose_sub = rospy.Subscriber(
            "servo_goal_pose", PoseStamped, self.pose_callback
        )

        # Rate of servoing updates
        self.rate = rospy.Rate(20)

    def pose_callback(self, pose: PoseStamped):
        """
        Sets goal pose from exponential filter

        Args:
            pose (PoseStamped): Input goal pose
        """
        self.servoer.update_pose(pose)

    def run(self):
        """
        Runs a loop around the servoer
        """
        while not rospy.is_shutdown():
            if self.servoer.goal_pose is None:
                self.rate.sleep()
            else:
                self.servoer.step()


if __name__ == "__main__":
    try:
        rospy.init_node("servo_pose_pid")
        servoing_manager = ServoToPose()

        servoing_manager.run()
    except rospy.ROSInterruptException:
        pass
