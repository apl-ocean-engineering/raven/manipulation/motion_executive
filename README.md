[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)

# motion_executive


[[_TOC_]]

## About
This repo contains nodes used to control the motion of the Bravo Arm.

Of the available nodes `interactive_marker.py` and  `servo_pose_pid.py` are basically demo nodes for testing servoing.

The others, `jog_joints.py` and `move_to_pose.py` are functional that geWnerate a GUI/CLI to control the bravo (either jogging the joints or planning to poses from a YAML file). `motion_server.py` creates action servers used to control the arm via a behaviour tree (see the `manip_executive`).

There are many other important resources that are not stand-alone nodes. The usage and functionality of two (`servo.py` and `mover.py`) will be covered in more detail.

## Node Usage

### motion_server

This node runs in conjunction with the behaviour tree node. Using several action servers, you can move the arm to a pose, jog joints, or servo to a pose.

### servo_demo

To run a servoing demo:
```bash
# If Simulating the arm:
roslaunch raven_manip_sw bravo_arm.launch simulated:=true

# If you want to use tChe real arm. See the raven_manip_sw repo for details
roslaunch raven_manip_sw bravo_arm.launch simulated:=false

# To use moveit_servo:
roslaunch motion_executive servo_demo.launch servo_method:=moveit

# To use custom implementation of resolved rate motion control:
roslaunch motion_executive servo_demo.launch servo_method:=rrmc

# To use custom implementation of manip maximization:
roslaunch motion_executive servo_demo.launch servo_method:=rrmc
```

You'll then need to move the arm into a position that is less near singularities and collisions as MoveIt tends to freak out.

![moveit_servo](assets/moveit_servo.mp4)

## Mover() and Servoer() Usage

### Mover() Class
The mover class is a wrapper around MoveIt functionality for the Bravo Arm and allows a simplified API to interface with the arm.

Instantiate a `mover` in any node you need access to the arm or arm information (current state of joints, frames of reference...) in by doing `mover = Mover()`.

### Servoer Class
We currently provide two servo instances to generate joint velocities from input position commands.

Each new Servoer needs to inherit from the parent class `Servoer` and define the `output_msg` attribute (probably either a TwistStamped or a JointTrajectory msg) a `publish_halt()` method to stop the arm and a `compute_output()` method to generate your `output_msg` from your input goal_pose.
