import numpy as np
from roboticstoolbox.robot.Robot import Robot


class Bravo(Robot):
    """
    Class that imports a UR5 URDF model

    ``UR3()`` is a class which imports a Universal Robotics UR5 robot
    definition from a URDF file.  The model describes its kinematic and
    graphical characteristics.

    .. runblock:: pycon

        >>> import roboticstoolbox as rtb
        >>> robot = rtb.models.URDF.UR5()
        >>> print(robot)

    Defined joint configurations are:

    - qz, zero joint angle configuration, 'L' shaped configuration
    - qr, vertical 'READY' configuration

    .. codeauthor:: Jesse Haviland
    .. sectionauthor:: Peter Corke
    """

    def __init__(self, urdf_path):
        links, name, urdf_string, urdf_filepath = self.URDF_read(urdf_path)

        super().__init__(
            links,
            name=name.upper(),
            manufacturer="Reach Robotics",
            urdf_string=urdf_string,
            urdf_filepath=urdf_filepath,
        )

        self.qr = np.array([np.pi, 0, 0, 0, np.pi / 2, 0])
        self.qz = np.zeros(6)
        self.qdlim = np.array([0.78, 0.78, 0.78, 0.78, 0.52, 0.52])

        self.addconfiguration("qr", self.qr)
        self.addconfiguration("qz", self.qz)
