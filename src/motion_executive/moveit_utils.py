"""
Collection of utility functions for dealing with MoveIt
Author(s):
Marc Micatka, micat001@uw.edu

University of Washington-APL 2023
"""

import time

import moveit_commander
import numpy as np
import pyikfast
import rospy
from geometry_msgs.msg import Point, Pose, PoseStamped, Quaternion
from moveit_msgs.msg import PositionIKRequest
from moveit_msgs.srv import GetPositionFK, GetPositionIK
from scipy.spatial.transform import Rotation
from std_msgs.msg import Header


def calculate_distance(pose1, pose2) -> float:
    """
    Calculate Pose - Pose distance.

    Args:
        pose1 (Pose or PoseStamped): pose 1
        pose2 (Pose or PoseStamped): pose 2

    Returns:
        float: distance
    """
    if isinstance(pose1, PoseStamped):
        pose1 = pose1.pose
    if isinstance(pose2, PoseStamped):
        pose2 = pose2.pose

    dx = pose1.position.x - pose2.position.x
    dy = pose1.position.y - pose2.position.y
    dz = pose1.position.z - pose2.position.z
    return np.sqrt(dx * dx + dy * dy + dz * dz)


def angle_axis_numpy(current_pose: np.array, desired_pose: np.array) -> float:
    """
    from: https://github.com/petercorke/robotics-toolbox-python
    """
    err = np.empty(6)
    err[:3] = desired_pose[:3, -1] - current_pose[:3, -1]
    rotation = desired_pose[:3, :3] @ current_pose[:3, :3].T
    li = np.array(
        [
            rotation[2, 1] - rotation[1, 2],
            rotation[0, 2] - rotation[2, 0],
            rotation[1, 0] - rotation[0, 1],
        ]
    )

    if np.allclose(li, 0):
        # diagonal matrix case
        if np.trace(rotation) > 0:
            # (1,1,1) case
            a_mat = np.zeros((3,))
        else:
            a_mat = np.pi / 2 * (np.diag(rotation) + 1)
    else:
        # non-diagonal matrix case
        ln = np.linalg.norm(li)
        a_mat = np.arctan2(ln, np.trace(rotation) - 1) * li / ln

    err[3:] = a_mat

    return err


def filter_for_ik_feas(
    poses: dict,
    move_group: moveit_commander.MoveGroupCommander,
    verbose=True,
    analytic=True,
) -> dict:
    """
    Filter input poses for ik feasibility using the MoveIt service call

    Args:
        poses (dict): Input poses, specified as [x, y, z, x, y, z, w]
        move_group (moveit_commander.MoveGroupCommander): Bravo MoveGroupCommander
        verbose (bool): Whether to print results or not
    Returns:
        dict: Filtered poses, only those kinematically reachable
    """
    result_poses = {}
    t_total = 0.0
    frame = move_group.get_planning_frame()

    if not analytic:
        ik_service = rospy.ServiceProxy("compute_ik", GetPositionIK)
        ik_request = PositionIKRequest()
        ik_request.group_name = move_group.get_name()  # arm
        ik_request.robot_state = move_group.get_current_state()
        ik_request.timeout = rospy.Duration(5.0)
        ik_request.avoid_collisions = False
        ik_request.ik_link_name = "ee_link"

    for k, v in poses.items():
        pose_goal = PoseStamped()
        pose_goal.header.frame_id = frame
        pose_goal.pose = Pose(position=Point(*v[:3]), orientation=Quaternion(*v[3:]))

        t0 = time.time()
        if analytic:
            rot_matrix = Rotation.from_quat(v[3:]).as_matrix()
            positions = v[:3]
            positions[-1] -= 0.1
            jt_pos = pyikfast.inverse(positions, list(rot_matrix.flatten()))

            if len(jt_pos) > 1:
                result_poses[k] = v
            else:
                print(f"Pose {k} is kinematically infeasible")
        else:
            ik_request.pose_stamped = pose_goal

            result = ik_service(ik_request=ik_request)
            if result.error_code.val == result.error_code.SUCCESS:
                result_poses[k] = v
            else:
                if verbose:
                    print(
                        f"Pose {k} is kinematically infeasible. {result.error_code.val}"
                    )
                else:
                    pass

        t1 = time.time() - t0
        t_total += t1
    t_total *= 1000
    if len(poses) and verbose:
        print(
            f"Total Time: {round(t_total, 2)} ms. Average: {round(t_total / len(poses), 2)} ms per run"
        )

    return result_poses


def filter_pose_for_ik_feas(
    pose_goal: PoseStamped,
    move_group: moveit_commander.MoveGroupCommander,
    verbose=False,
):
    ik_service = rospy.ServiceProxy("compute_ik", GetPositionIK)
    ik_request = PositionIKRequest()
    ik_request.group_name = move_group.get_name()
    ik_request.robot_state = move_group.get_current_state()
    ik_request.timeout = rospy.Duration(5.0)
    ik_request.avoid_collisions = False

    ik_request.pose_stamped = pose_goal
    result = ik_service(ik_request=ik_request)
    if result.error_code.val == result.error_code.SUCCESS:
        if verbose:
            print("Pose is kinematically feasible.")
        return True
    else:
        if verbose:
            print("Pose is kinematically infeasible.")
        else:
            return False


def pose_from_joint_angles(
    joint_angles: list,
    move_group: moveit_commander.MoveGroupCommander,
    desired_link: str,
) -> PoseStamped:
    """
     From a list of joint positions, get pose of robot link

    Args:
        joint_angles (list): List of joint angles for robot (G-B)
        move_group (moveit_commander.MoveGroupCommander): Arm Move Group
        desired_link (str, optional): Link to get pose of. Defaults to "ee_link".

    Returns:
        PoseStamped: output pose
    """
    fk_service = rospy.ServiceProxy("compute_fk", GetPositionFK)

    planning_frame = move_group.get_planning_frame()
    joint_names = move_group.get_active_joints()
    robot_state = move_group.get_current_state()
    joint_state = robot_state.joint_state
    joint_state.name = joint_names

    # Joint angles come in reversed (in G-B order) but need to be set in (B-G) order.
    joint_state.position = joint_angles[::-1]
    header = Header(frame_id=planning_frame)
    fk_response = fk_service(header, [desired_link], robot_state)
    pose_stamped = fk_response.pose_stamped[0]

    return pose_stamped
