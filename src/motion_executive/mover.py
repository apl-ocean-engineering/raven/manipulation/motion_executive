"""
Wrapper around MoveIt functionality.
Convenience functions for planning and executing trajectories
and accessing robot state.

Author(s):
Tim Player, playertr@uw.edu
Marc Micatka, micat001@uw.edu

University of Washington-APL 2023
"""

import sys
import time
from typing import Optional

import moveit_commander
import numpy as np
import rospy
from geometry_msgs.msg import PoseStamped
from moveit_msgs.msg import MotionPlanResponse
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint

from motion_executive.moveit_utils import calculate_distance, pose_from_joint_angles


class Mover:
    """Wrapper around MoveIt functionality for the arm."""

    arm_move_group_cmdr: moveit_commander.MoveGroupCommander
    hand_move_group_cmdr: moveit_commander.MoveGroupCommander
    arm_robot_cmdr: moveit_commander.RobotCommander
    arm_group_name: str
    scene: moveit_commander.PlanningSceneInterface
    grasping_group_name: str

    def __init__(
        self,
        grasping_group_name="hand",
        arm_group_name="arm",
        robot_description="robot_description",
    ) -> None:
        # spin up MoveIt python API
        moveit_commander.roscpp_initialize(sys.argv)

        self.arm_group_name = arm_group_name
        self.grasping_group_name = grasping_group_name
        self.arm_robot_cmdr = moveit_commander.RobotCommander(
            robot_description=robot_description
        )
        self.arm_move_group_cmdr = moveit_commander.MoveGroupCommander(
            self.arm_group_name, robot_description=robot_description
        )
        self.arm_move_group_cmdr.set_planner_id("RRTConnect")

        self.hand_move_group_cmdr = moveit_commander.MoveGroupCommander(
            self.grasping_group_name, robot_description=robot_description
        )
        self.gripper_pub = rospy.Publisher(
            "hand_velocity_controller/command", data_class=JointTrajectory, queue_size=1
        )

        # we use this Scene to modify the collision objects programmatically
        self.scene = moveit_commander.PlanningSceneInterface(synchronous=True)

        self.success_distance_threshold = 0.01
        self.ee_axis_name = "bravo_axis_a"
        self.gripper_closed, self.gripper_open = self.arm_robot_cmdr.get_joint(
            self.ee_axis_name
        ).bounds()

        # This uses the 1-based joint numbering from the BPL protocol
        # 1: bravo_joint_a (Linear Jaws)
        # 2: bravo_joint_b (Rotate End Effector)
        # 3: bravo_joint_c (Bend Forearm)
        # 4: bravo_joint_d (Rotate Elbow)
        # 5: bravo_joint_e (Bend Elbow)
        # 6: bravo_joint_f (Bend Shoulder)
        # 7: bravo_joint_g (Rotate Base)
        # And mirrors the joints from raven_manip_msgs/MoveJoint
        self.joint_mapping = {
            7: "bravo_axis_g",
            6: "bravo_axis_f",
            5: "bravo_axis_e",
            4: "bravo_axis_d",
            3: "bravo_axis_c",
            2: "bravo_axis_b",
            1: "bravo_axis_a",
        }

    def get_ee_pose(self, verbose=False) -> PoseStamped:
        """
        Return pose of end effector

        Returns:
            PoseStamped: pose of ee
        """
        curr_pose = self.arm_move_group_cmdr.get_current_pose()

        if verbose:
            x = curr_pose.pose.position.x
            y = curr_pose.pose.position.y
            z = curr_pose.pose.position.z
            qx = curr_pose.pose.orientation.x
            qy = curr_pose.pose.orientation.y
            qz = curr_pose.pose.orientation.z
            qw = curr_pose.pose.orientation.w
            print(
                f"[{x:.4f}, {y:.4f}, {z:.4f}, {qx:.4f}, {qy:.4f}, {qz:.4f}, {qw:.4f}]"
            )
        return curr_pose

    def get_pose_of_named_state(
        self, named_state: str, link="ee_link"
    ) -> Optional[PoseStamped]:
        """
        Get a PoseStamped message of a named position

        Args:
            state (str): named position
            link (str): link to get pose of. Defaults to "ee_link"

        Returns:
            PoseStamped: Pose of link
        """
        if named_state in self.arm_move_group_cmdr.get_named_targets():
            joint_values = self.arm_move_group_cmdr.get_named_target_values(named_state)
            pose_stamped = pose_from_joint_angles(
                list(joint_values.values()), self.arm_move_group_cmdr, link
            )
            return pose_stamped
        else:
            return None

    def get_current_joint_values(self) -> dict:
        """
        Returns a dictionary with names and states of joints.
        Required because of how moveit separates groups of joints (arm separate from hand)

        Returns:
            state_dict (dict): dictionary of joint names: position
        """
        joint_state = self.arm_move_group_cmdr.get_current_state().joint_state
        state_dict = {}
        for name, pos in zip(joint_state.name, joint_state.position):
            state_dict[name] = pos
        return state_dict

    def get_joint_value(self, joint_index: int) -> float:
        """
        Simple get function that returns a single position for a single joint

        Args:
            joint_index (int): Which joint to retrieve

        Returns:
            float: Current position
        """
        # Confusingly, these values get printed as a dictionary and we index by value starting at 1.
        # Cleanest approach is to use a custom defined dictionary mapping
        joint_name = self.joint_mapping[joint_index]
        return self.get_current_joint_values()[joint_name]

    def move_joints(self, joints: np.ndarray, wait: bool = False):
        """
        Move robot to the given joint configuration.

        Args:
            joints (np.ndarray): joint angles
            wait (bool): whether to block until finished
        """
        success = self.arm_move_group_cmdr.go(joints, wait=wait)
        self.arm_move_group_cmdr.stop()
        return success

    def move_joint(self, joint_index: int, position: float, wait: bool = False):
        """
        Move joints based on string name and desired position

        Args:
            joint_index (int): index of joint
            position (float): desired position of joint
            wait (bool, optional): Blocking or non-blocking move. Defaults to False.
        """
        if joint_index == 1:
            try:
                self.move_gripper(position, wait)
                return True
            except Exception as ex:
                rospy.logerr(f"{ex}")
                return False

        else:
            # Get the current joint values
            joint_values = self.arm_move_group_cmdr.get_current_joint_values()

            # Update the target joint value in the current_joint_values list
            joint_values[joint_index] = position

            # Plan and execute the trajectory to the new joint configuration
            self.arm_move_group_cmdr.set_joint_value_target(joint_values)
            success, _, _, _ = self.arm_move_group_cmdr.plan()
            if not success:
                return False
            try:
                self.arm_move_group_cmdr.go(joint_values, wait)
                return True

            except Exception as ex:
                rospy.logerr(f"{ex}")
                return False

    def get_gripper_position(self) -> float:
        """
        Returns:
            float: Gripper position (mm)
        """
        try:
            joint_position = self.get_current_joint_values()[self.ee_axis_name]
            return joint_position

        except KeyError:
            print(f"Joint '{self.ee_axis_name}' not found in the robot's joint state.")
            return 0

    def move_gripper(self, pos: float, wait: bool = False) -> bool:
        """
        Move the gripper fingers to the given actuator value.

        Args:
            pos (float): desired position of self.ee_axis_name
            wait (bool): whether to wait until it's probably done
        """

        joint_state = self.arm_move_group_cmdr.get_current_state().joint_state
        # Find the index of the joint by its name
        try:
            joint_index = joint_state.name.index(self.ee_axis_name)
            joint_position = joint_state.position[joint_index]
        except ValueError:
            return False

        if np.abs(pos - joint_position) <= 0.001:
            return True
        elif pos > self.gripper_open or pos < self.gripper_closed:
            return False

        jt = JointTrajectory()
        jt.joint_names = [self.ee_axis_name]
        jt.header.stamp = rospy.Time.now()

        jtp = JointTrajectoryPoint()
        jtp.positions = [pos]
        jtp.time_from_start = rospy.Duration(secs=3)
        jt.points.append(jtp)

        self.gripper_pub.publish(jt)

        if wait:
            time.sleep(3)

        return True

    def move_ee_to_pose(self, pose: PoseStamped, wait: bool = False) -> bool:
        """
        Move the end effector to the given pose.

        Args:
            pose (geometry_msgs.msg.Pose): desired pose for end effector
            wait (bool): whether to block until finished
        """

        pose.header.stamp = rospy.Time.now()  # the timestamp may be out of date.
        self.arm_move_group_cmdr.set_pose_target(pose, end_effector_link="ee_link")

        motion_goal_pub = rospy.Publisher("motion_goal", PoseStamped, queue_size=10)
        motion_goal_pub.publish(pose)

        self.arm_move_group_cmdr.go(wait=wait)
        distance = np.abs(calculate_distance(self.get_ee_pose().pose, pose.pose))

        # print(f"Ended up: {1000*distance:.3f}mm away")

        self.arm_move_group_cmdr.stop()
        self.arm_move_group_cmdr.clear_pose_targets()

        return distance < self.success_distance_threshold

    def plan_to_named_state(self, state: str) -> MotionPlanResponse:
        """
        Generate a plan to a named stated, but don't move there!

        Args:
            state (str): Named state (from SRDF)

        Returns:
            MotionPlanResponse: Planning response
        """
        # Set the target pose to the named position
        self.arm_move_group_cmdr.set_named_target(state)
        planning_response = self.arm_move_group_cmdr.plan()
        return planning_response

    def move_to_named_state(self, state: str, wait: bool = False) -> bool:
        """
        Move the arm group to a named state from the SRDF.

        Args:
            state (str): the name of the state
            wait (bool, optional): whether to block until finished. Defaults to True.
        """
        states = self.arm_move_group_cmdr.get_named_targets()
        if state in states:
            self.arm_move_group_cmdr.set_named_target(state)
            success = self.arm_move_group_cmdr.go(wait=wait)
            self.arm_move_group_cmdr.stop()
            return success

        else:
            print(f"Requested state {state} not in Named States!")
            return False

    def set_velocity_scale(self, new_scale: float = 0.1):
        """
        Sets velocity scaling

        Args:
            new_scale (float, optional): Velocity scaling factor. Defaults to 0.1.
        """
        self.arm_move_group_cmdr.set_max_velocity_scaling_factor(new_scale)

    def get_velocity_scale(self):
        """
        Returns current velocity scaling
        """
        return self.arm_move_group_cmdr.get_max_velocity_scaling_factor()

    def execute_grasp_open_loop(
        self, goal_grasp: PoseStamped, goal_orbital_grasp: PoseStamped
    ) -> bool:
        """
        Execute an open loop grasp by planning and executing a sequence of motions in turn:
            1) Open the jaws
            2) Move the end effector the the orbital pose
            3) Move the end effector to the final pose
            4) Close the jaws
            5) Move the end effector to the orbital pose
            6) Move the arm to the 'rest' configuration

        Args:
            orbital_pose (PoseStamped): the pre-grasp orbital pose of the end effector
            final_pose (PoseStamped): the end effector pose in which to close the jaws

        Returns:
            bool: whether every step succeeded according to MoveIt
        """
        print("Executing grasp.")
        print("\tOpening jaws.")

        success = self.move_gripper(self.gripper_open, wait=False)
        if not success:
            return False

        print("\tMoving end effector to orbital pose.")
        success = self.move_ee_to_pose(goal_orbital_grasp, wait=False)
        if not success:
            return False

        print("\tMoving end effector to final pose.")
        success = self.move_ee_to_pose(goal_grasp, wait=False)
        if not success:
            return False

        print("\tClosing jaws.")
        success = self.move_gripper(self.gripper_closed, wait=False)
        if not success:
            return False

        rospy.sleep(2)
        return success
