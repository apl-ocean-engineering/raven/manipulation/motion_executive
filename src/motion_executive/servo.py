# servo.py
# Classes and utility functions for supporting end effector servoing
import copy
import math
import os
from typing import Tuple

import numpy as np
import qpsolvers as qp
import roboticstoolbox as rtb
import rospkg
import rospy
import tf2_ros
from geometry_msgs.msg import TwistStamped, Vector3
from scipy.spatial.transform import Rotation, Slerp
from std_msgs.msg import Float32MultiArray
from tf2_geometry_msgs import PoseStamped
from trajectory_msgs.msg import JointTrajectory

from motion_executive.bravo_robot import Bravo
from motion_executive.conversions import jt_traj_from_vel_arr, matrix_from_posestamped
from motion_executive.moveit_utils import angle_axis_numpy
from motion_executive.mover import Mover

rospack = rospkg.RosPack()
pkg_root = rospack.get_path("motion_executive")


class Servoer:
    """
    Parent class Servoer.

    Needs to be inherited and implemented for individual methods
    """

    def __init__(self, pid_params: dict, ee_frame_id: str) -> None:
        # goal_pose gets updated with each call to "update"
        self.goal_pose = None
        self.twist = None
        self.output_msg = None
        self.publish_period = 0.1
        self.rate = rospy.Rate(1 / self.publish_period)

        # End-Effector Frame
        self.ee_frame_id = ee_frame_id

        # Dictionary of PID terms for xyz and rpy
        self.xyz_terms = pid_params["xyz_terms"]
        self.rpy_terms = pid_params["rpy_terms"]
        self.goal_position_tolerance = pid_params["goal_position_tolerance"]
        self.goal_orientation_tolerance = pid_params["goal_orientation_tolerance"]

        # Setup the expo filter:
        self.goal_pose_filter = PoseStampedExpoFilter(
            tau=(self.xyz_terms["tau"], self.rpy_terms["tau"])
        )
        # Setup Publishers:
        # Low-pass-filtered goal pose for introspection
        self.lpf_goal_pose_pub = rospy.Publisher(
            "~lpf_goal_pose", data_class=PoseStamped, queue_size=10
        )

        self.twist_pub = rospy.Publisher(
            name="~twist_output",
            data_class=TwistStamped,
            queue_size=10,
        )

        # Require an output_publisher attribute:

        # Setup PID controller
        kp = (self.xyz_terms["kp"], self.rpy_terms["kp"])
        ki = (self.xyz_terms["ki"], self.rpy_terms["ki"])
        kd = (self.xyz_terms["kd"], self.rpy_terms["kd"])
        self.ee_pid = EndEffectorPID(self.ee_frame_id, kp, ki, kd)

    def publish_halt(self) -> None:
        """
        Define a halt function that stops arm motion
        """
        return None

    def update_pose(self, pose: PoseStamped):
        """
        Publishes goal pose from exponential filter

        Args:
            pose (PoseStamped): Input goal pose
        """
        self.goal_pose = self.goal_pose_filter.update(pose)

        if self.goal_pose is not None:
            self.goal_pose.header.stamp = rospy.Time(0)
            self.lpf_goal_pose_pub.publish(self.goal_pose)

    def compute_output(self) -> Tuple[TwistStamped, TwistStamped]:
        """
        Define a function to take a goal pose and output
        the required msg type (either a TwistStamped or JointTrajectory)

        For some servoing methods, they don't return a twist.
        For introspection, we want that twist!
        So the first return is whatever msg we need for the output, second is ALWAYS a
        """
        twist = self.ee_pid.compute_twist(self.goal_pose)

        return (twist, twist)

    def step(self) -> bool:
        """
        Runs a single update cycle of the servoer

        Returns:
            bool: Whether it's still running
        """
        if self.goal_pose is None:
            self.publish_halt()
            return False

        # Calculate desired twist from EE PID
        try:
            self.output_msg, twist_msg = self.compute_output()
            self.twist_pub.publish(twist_msg)
            if not self.arrived():
                rospy.loginfo(
                    f"Position Error: {self.ee_pid.current_position_error():.3f}m, Orientation Error: {self.ee_pid.current_orientation_error():.3f}"
                )
                self.output_pub.publish(self.output_msg)
                return True
            elif self.arrived():
                rospy.loginfo("Reached Pose, Halting!")
                self.publish_halt()
                return False
        except Exception as ex:
            rospy.logwarn(ex)
            self.publish_halt()
            return False

    def arrived(self) -> bool:
        """
        Arrived at goal pose?

        Returns:
            bool: Arrived at goal pose?
        """
        return (
            self.ee_pid.current_position_error() <= self.goal_position_tolerance
            and self.ee_pid.current_orientation_error()
            <= self.goal_orientation_tolerance
        )


class MoveItServo(Servoer):
    """
    Uses MoveIt servo. Publishes a TwistStamped message
    MoveIt servo subscribes to the Twist and outputs a JointTrajectory
    """

    def __init__(self, pid_params: dict, ee_frame_id: str) -> None:
        super().__init__(pid_params, ee_frame_id)

        # Output topic to moveit_servo
        # Called "output_pub"
        self.output_pub = rospy.Publisher(
            name="~servo_output",
            data_class=TwistStamped,
            queue_size=10,
        )

    def publish_halt(self) -> None:
        """
        Halt motion of servoing by publishing a zero twist.
        Reset Goal Pose
        """
        zero_twist = TwistStamped()
        zero_twist.header.frame_id = self.ee_frame_id
        self.output_pub.publish(zero_twist)
        self.twist_pub.publish(zero_twist)
        self.goal_pose = None


class RRMCServo(Servoer):
    """
    Uses Robotics Toolbox to calculate joint velocities.
    Publishes a JointTrajectory message directly.

    Simple implementation of resolved rate motion control
    """

    def __init__(self, pid_params: dict, ee_frame_id: str) -> None:
        super().__init__(pid_params, ee_frame_id)

        self.mover = Mover()
        self.init_run = True

        # Joint Names (necessary for JT message)
        self.joint_names = [
            "bravo_axis_g",
            "bravo_axis_f",
            "bravo_axis_e",
            "bravo_axis_d",
            "bravo_axis_c",
            "bravo_axis_b",
        ]
        self.robot = Bravo(os.path.join(pkg_root, "urdf/bravo.urdf"))
        self.joint_num = 6

        # Output topic to moveit_servo
        self.output_pub = rospy.Publisher(
            name="~servo_output",
            data_class=JointTrajectory,
            queue_size=10,
        )

    def compute_output(self) -> TwistStamped:
        """
        Define a function to take a goal pose and output
        the required msg type (either a TwistStamped or JointTrajectory)
        """
        twist = self.ee_pid.compute_twist(self.goal_pose)

        velocities = -1 * np.array(
            [
                twist.twist.linear.x,
                twist.twist.linear.y,
                twist.twist.linear.z,
                twist.twist.angular.x,
                twist.twist.angular.y,
                twist.twist.angular.z,
            ]
        )

        positions = np.array(
            self.mover.arm_move_group_cmdr.get_current_state().joint_state.position[0:6]
        )
        # Twist is in the EE frame. We get the jacobian in the EE frame as well
        # Note that the moveit implementation gets the jacobian in the BASE planning frame
        qd = np.linalg.pinv(self.robot.jacobe(positions)) @ velocities

        # When sending out trajectory_msgs/JointTrajectory type messages, the "trajectory" is just a single point.
        # That point cannot have the same timestamp as the start of trajectory execution since that would mean the
        # arm has to reach the first trajectory point the moment execution begins. To prevent errors about points
        # being 0 seconds in the past, the smallest supported timestep is added as time from start to the trajectory point.
        # Comment copied from https://github.com/moveit/moveit/blob/master/moveit_ros/moveit_servo/src/servo_calcs.cpp
        if self.init_run:
            time_from_start = rospy.Duration(nsecs=100000000)
        else:
            time_from_start = rospy.Duration(nsecs=1)

        output = jt_traj_from_vel_arr(
            velocities=qd[: self.joint_num],
            positions=positions,
            joint_names=self.joint_names,
            time_from_start=time_from_start,
        )
        return output, twist

    def publish_halt(self) -> None:
        """
        Halt motion of servoing by publishing a zero twist/JT
        Reset Goal Pose
        """
        positions = (
            self.mover.arm_move_group_cmdr.get_current_state().joint_state.position[0:6]
        )
        trajectory_msg = jt_traj_from_vel_arr(
            velocities=6 * [0.0],
            positions=positions,
            joint_names=self.joint_names,
            time_from_start=rospy.Duration(nsecs=1),
        )
        self.output_pub.publish(trajectory_msg)
        self.goal_pose = None

        zero_twist = TwistStamped()
        zero_twist.header.frame_id = self.ee_frame_id
        self.twist_pub.publish(zero_twist)


class MMCServo(Servoer):
    """
    Uses Robotics Toolbox to calculate joint velocities.
    Publishes a JointTrajectory message directly.

    From:
    https://jhavl.github.io/mmc/

    @article{haviland2020mmc,
            title={A purely-reactive manipulability-maximising motion controller},
            author={Haviland, J and Corke, P},
            journal={arXiv preprint arXiv:2002.11901},
            year={2020}}
    """

    def __init__(self, pid_params: dict, ee_frame_id: str) -> None:
        super().__init__(pid_params, ee_frame_id)

        self.mover = Mover()
        self.init_run = True

        # Joint Names (necessary for JT message)
        self.joint_names = [
            "bravo_axis_g",
            "bravo_axis_f",
            "bravo_axis_e",
            "bravo_axis_d",
            "bravo_axis_c",
            "bravo_axis_b",
        ]
        self.robot = Bravo(os.path.join(pkg_root, "urdf/bravo.urdf"))

        # Gain term (lambda) for control minimisation
        self.Y = 0.01
        self.joint_num = 6

        # Output topic to moveit_servo
        self.output_pub = rospy.Publisher(
            name="~servo_output",
            data_class=JointTrajectory,
            queue_size=10,
        )

    def compute_output(self) -> TwistStamped:
        """
        Define a function to take a goal pose and output
        the required msg type (either a TwistStamped or JointTrajectory)
        """
        # The pose of the Panda's end-effector
        te = matrix_from_posestamped(self.mover.get_ee_pose())
        tep = matrix_from_posestamped(self.goal_pose)

        # Transform from the end-effector to desired pose
        etep = np.linalg.pinv(te) * tep

        # Spatial error
        rotation = Rotation.from_matrix(np.array(etep)[:3, :3]).as_euler("xyz")
        translation = np.array(etep)[:3, 3]
        e = np.sum(np.abs(np.r_[translation, rotation * np.pi / 180]))

        # Calulate the required end-effector spatial velocity for the robot
        # to approach the goal. Gain is set to 1.0
        v, arrived = rtb.p_servo(te, tep, 1.0)

        twist = self.ee_pid.compute_twist(self.goal_pose)
        twist.twist.linear.x = v[0]
        twist.twist.linear.y = v[1]
        twist.twist.linear.z = v[2]
        twist.twist.angular.x = v[3]
        twist.twist.angular.y = v[4]
        twist.twist.angular.z = v[5]

        positions = np.array(
            self.mover.arm_move_group_cmdr.get_current_state().joint_state.position[0:6]
        )
        # print(f"{e:.3f}, {self.ee_pid.current_error():.3f}")
        e = self.ee_pid.current_error()

        #  Quadratic component of objective function
        quad = np.eye(self.joint_num + 6)

        # Joint velocity component of Q
        quad[: self.joint_num, : self.joint_num] *= self.Y

        # Slack component of Q
        quad[self.joint_num :, self.joint_num :] = (1 / e) * np.eye(6)

        # The equality contraints
        a_eq = np.c_[self.robot.jacobe(positions), np.eye(6)]
        b_eq = v.reshape((6,))

        # The inequality constraints for joint limit avoidance
        a_in = np.zeros((self.joint_num + 6, self.joint_num + 6))
        b_in = np.zeros(self.joint_num + 6)

        # The minimum angle (in radians) in which the joint is allowed to approach
        # to its limit
        ps = 0.05

        # The influence angle (in radians) in which the velocity damper
        # becomes active
        pi = 0.9

        # Form the joint limit velocity damper
        (
            a_in[: self.joint_num, : self.joint_num],
            b_in[: self.joint_num],
        ) = self.robot.joint_velocity_damper(ps, pi, self.joint_num)

        # Linear component of objective function: the manipulability Jacobian
        c = np.r_[
            -self.robot.jacobm(J=self.robot.jacobe(positions)).reshape(
                (self.joint_num,)
            ),
            np.zeros(6),
        ]

        # The lower and upper bounds on the joint velocity and slack variable
        lb = -np.r_[self.robot.qdlim[: self.joint_num], 10 * np.ones(6)]
        ub = np.r_[self.robot.qdlim[: self.joint_num], 10 * np.ones(6)]

        # Solve for the joint velocities dq
        qd = qp.solve_qp(quad, c, a_in, b_in, a_eq, b_eq, lb=lb, ub=ub, solver="cvxopt")

        # This might not be necessary but matches the MoveIt JT publications
        if self.init_run:
            time_from_start = rospy.Duration(nsecs=100000000)
        else:
            time_from_start = rospy.Duration(nsecs=1)

        output = jt_traj_from_vel_arr(
            velocities=qd[: self.joint_num],
            positions=positions,
            joint_names=self.joint_names,
            time_from_start=time_from_start,
        )
        return output, twist

    def publish_halt(self) -> None:
        """
        Halt motion of servoing by publishing a zero twist.
        Reset Goal Pose
        """
        positions = (
            self.mover.arm_move_group_cmdr.get_current_state().joint_state.position[0:6]
        )
        trajectory_msg = jt_traj_from_vel_arr(
            velocities=6 * [0.0],
            positions=positions,
            joint_names=self.joint_names,
            time_from_start=rospy.Duration(nsecs=1),
        )
        self.output_pub.publish(trajectory_msg)
        self.goal_pose = None

        zero_twist = TwistStamped()
        zero_twist.header.frame_id = self.ee_frame_id
        self.twist_pub.publish(zero_twist)


class EndEffectorPID:
    """
    Controller to issue a Twist driving the end effector to a desired pose.

    MoveIt servo takes an end effector Twist (linear + rotational velocities)
    and converts it to joint commands (position, velocity).

    This controller implements a PID loop around end effector pose
    (error) to a goal pose.
    """

    def __init__(
        self,
        ee_frame_id: str,
        kp=(5, 20),
        ki=(0, 0),
        kd=(0, 0),
        integral_max=2,
    ):
        """
        Args:
            ee_frame_id (string):   /tf frame of the end effector (the joint
            being controlled)
        """

        self.ee_frame_id = ee_frame_id

        # PID values are unique to translation and rotation components
        self.set_params(kp, ki, kd)

        self.tf_buffer = tf2_ros.Buffer()
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer)
        self._current_error = [0, 0, 0, 0, 0, 0]
        self._current_orientation_error = 0.0

        self.integral_max = integral_max
        self.prev_error = [0, 0, 0, 0, 0, 0]
        self.integral = [0, 0, 0, 0, 0, 0]

        self.n = len(self.kp)

        self.error_pub = rospy.Publisher(
            name="~pid_error",
            data_class=Float32MultiArray,
            queue_size=10,
        )
        self.effort_pub = rospy.Publisher(
            name="~pid_efforts",
            data_class=Float32MultiArray,
            queue_size=10,
        )

    def set_params(self, kp: tuple, ki: tuple, kd: tuple):
        self.kp = (kp[0], kp[0], kp[0], kp[1], kp[1], kp[1])
        self.ki = (ki[0], ki[0], ki[0], ki[1], ki[1], ki[1])
        self.kd = (kd[0], kd[0], kd[0], kd[1], kd[1], kd[1])

    def compute_twist(self, target_pose: PoseStamped) -> TwistStamped:
        """Generate visual servoing Twist command using proportional control over X,Y,Z,R,P,Y .

        Args:
            target_pose (PoseStamped): the pose of the target in the frame of the end effector

        Returns:
            TwistStamped: the twist to issue to MoveIt servo
        """
        # Transform the target pose into the end-effector frame for servoing.
        # In the end effector frame, the target pose also represents the error
        # between the desired and current end effector pose.

        target_pose = self.tf_buffer.transform(target_pose, self.ee_frame_id)

        # convert to [x,y,z,r,p,y]
        self._current_error = self._posestamped_to_arr(target_pose)

        # Calculate angle-axis error:
        self._current_orientation_error = angle_axis_numpy(
            np.eye(4), matrix_from_posestamped(target_pose)
        )

        self.error_pub.publish(Float32MultiArray(data=list(self._current_error)))

        efforts = self.update(self._current_error)
        self.effort_pub.publish(Float32MultiArray(data=list(efforts)))

        twist = TwistStamped()
        twist.header = target_pose.header

        # make current so servo command doesn't time out
        twist.header.stamp = rospy.Time.now()
        twist.twist.linear = Vector3(*efforts[:3])
        twist.twist.angular = Vector3(*efforts[3:])

        return twist

    def update(self, error):
        for i in range(self.n):
            self.integral[i] += error[i]
            self.integral[i] = max(
                min(self.integral[i], self.integral_max), -self.integral_max
            )

        p_term = [self.kp[i] * error[i] for i in range(self.n)]
        i_term = [self.ki[i] * self.integral[i] for i in range(self.n)]
        d_term = [self.kd[i] * (error[i] - self.prev_error[i]) for i in range(self.n)]

        output = [p_term[i] + i_term[i] + d_term[i] for i in range(self.n)]

        self.prev_error = error

        return output

    def current_position_error(self) -> float:
        """
        Euclidean distance between current pose and goal pose.

        Returns:
            float: distance
        """
        return np.sqrt(
            self._current_error[0] ** 2
            + self._current_error[1] ** 2
            + self._current_error[2] ** 2
        )

    def current_orientation_error(self) -> float:
        """
        Axis-Angle error between current pose and goal pose

        Returns:
            float: orientation error (axis angle)
        """

        return np.sum(np.abs(self._current_orientation_error))

    def current_error(self) -> float:
        error = np.sum(np.abs(self._current_error))

        return error

    @staticmethod
    def _posestamped_to_arr(ps: PoseStamped) -> np.ndarray:
        """Convert a PoseStamped to np.array([x,y,z,r,p,y])"""
        pos, quat = ps.pose.position, ps.pose.orientation
        rpy = Rotation.from_quat([quat.x, quat.y, quat.z, quat.w]).as_euler("xyz")
        return np.array([pos.x, pos.y, pos.z, *rpy])


class ExpoFilter:
    """One-dimensional (scalar) exponential FIR filter."""

    def __init__(self, tau: float = 0):
        self.tau = tau
        self.x = float("nan")

    def update(self, x: float) -> float:
        if math.isnan(self.x):
            self.x = x
        else:
            self.x = self.tau * self.x + (1 - self.tau) * x

        return self.x


class PoseStampedExpoFilter:
    """Exponential filter on a PoseStamped that applies an expo filter to each
    scalar term individually.

    Rotation uses spherical linear interpolation."""

    def __init__(self, tau=(0.9, 0.9)):
        self.x_filter = ExpoFilter()
        self.y_filter = ExpoFilter()
        self.z_filter = ExpoFilter()
        self.orientation = Rotation.identity()
        self.set_tau(tau)

        self.is_initialized = False

    def uninitialize(self):
        self.is_initialized = False

    def set_tau(self, tau):
        self.x_filter.tau = tau[0]
        self.y_filter.tau = tau[0]
        self.z_filter.tau = tau[0]
        self.orientation_tau = tau[1]

    def update(self, posestamped: PoseStamped) -> PoseStamped:
        if self.is_initialized:
            self.x_filter.update(posestamped.pose.position.x)
            self.y_filter.update(posestamped.pose.position.y)
            self.z_filter.update(posestamped.pose.position.z)

            # lowpass filter using spherical linear interpolation
            orn = posestamped.pose.orientation
            orn = Rotation.from_quat([orn.x, orn.y, orn.z, orn.w])
            self.orientation = Slerp(
                times=[0, 1],
                rotations=Rotation.from_quat(
                    [self.orientation.as_quat(), orn.as_quat()]
                ),  # gross
            )(1 - self.orientation_tau)
        else:
            self.reset(posestamped)
            self.is_initialized = True

        ps = copy.deepcopy(posestamped)
        ps.pose.position.x = self.x_filter.x
        ps.pose.position.y = self.y_filter.x
        ps.pose.position.z = self.z_filter.x
        ps.pose.orientation.x = self.orientation.as_quat()[0]
        ps.pose.orientation.y = self.orientation.as_quat()[1]
        ps.pose.orientation.z = self.orientation.as_quat()[2]
        ps.pose.orientation.w = self.orientation.as_quat()[3]
        return ps

    def reset(self, posestamped: PoseStamped):
        self.x_filter.x = posestamped.pose.position.x
        self.y_filter.x = posestamped.pose.position.y
        self.z_filter.x = posestamped.pose.position.z

        orn = posestamped.pose.orientation
        self.orientation = Rotation.from_quat([orn.x, orn.y, orn.z, orn.w])


servo_method_dict = {"moveit": MoveItServo, "rrmc": RRMCServo, "mmc": MMCServo}
