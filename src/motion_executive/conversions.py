# Standard imports
from typing import Tuple

import matplotlib
import matplotlib.colors
import moveit_commander
import numpy as np
import rospy
import yaml
from geometry_msgs.msg import Pose, Quaternion
from moveit_msgs.srv import GetPositionFK
from nav_msgs.msg import Path
from rospy import Duration
from scipy.spatial.transform import Rotation
from std_msgs.msg import ColorRGBA, Header
from tf2_geometry_msgs import PoseStamped
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from visualization_msgs.msg import Marker


def read_yaml_file(file_path: str) -> dict:
    """
    Read yaml as dictionary

    Args:
        file_path (str): path to yaml file

    Returns:
        dict: yaml loaded as dictionary (if no error)
    """
    try:
        with open(file_path, encoding="utf-8") as stream:
            data = yaml.safe_load(stream)
            return data
    except (yaml.YAMLError, FileNotFoundError) as ex:
        rospy.logerr(f"Error reading YAML file: {ex}")
        return {}


def poses_from_yaml(file_path: str) -> dict:
    """
    Filter dictionary to remove non-floats

    Args:
        file_path (str): path to yaml file

    Returns:
        dict: yaml loaded as dictionary (if no error)
    """
    positions = read_yaml_file(file_path)
    filtered_positions = {}
    if positions is not None:
        for k, v in positions.items():
            if all(isinstance(elem, (float, int)) for elem in v):
                filtered_positions[k] = v
            else:
                print(f"Not all items in {k} are numbers.")
    return filtered_positions


def get_ros_colors(num_colors: int, cmap_name="rainbow") -> list:
    """
    Get colors for markers based on equally spaced intervals in colormap.

    Args:
        num_colors (int): Number of colors to generate
        cmap_name (str, optional): Colormap string. Defaults to "rainbow".

    Returns:
        List [ColorRGBA]: list of ColorRGBA colors
    """
    try:
        cmap = matplotlib.colormaps[cmap_name]
    except KeyError:
        cmap = matplotlib.colormaps["rainbow"]
    colors = [cmap(i / (num_colors)) for i in range(num_colors)]
    rgba_values = [matplotlib.colors.to_rgba(color) for color in colors]
    ros_colors = [
        ColorRGBA(r=val[0], g=val[1], b=val[2], a=val[3]) for val in rgba_values
    ]

    return ros_colors


def markers_from_pose(
    pose: Pose, label: str, unique_id: int, color: ColorRGBA, frame: str
) -> Tuple[Marker, Marker]:
    """
     Returns a marker and text marker from a pose.
    Args:
        pose (Pose): Pose of marker/texts
        label (str): Text to display
        unique_id (int): id (unique ID of marker)
        color (ColorRGBA): marker color
        frame (str): frame to draw marker in

    Returns:
        Tuple(marker, text_marker) (Marker, Marker): Marker for pose, marker for text
    """
    marker = Marker()
    marker.header.frame_id = frame
    marker.type = Marker.SPHERE
    marker.pose = pose
    marker.scale.x = 0.025
    marker.scale.y = 0.025
    marker.scale.z = 0.025
    marker.color = color
    marker.id = 2 * unique_id
    marker.lifetime = rospy.Duration(0)
    text_pose = Pose()
    text_pose.position.x = pose.position.x
    text_pose.position.y = pose.position.y
    text_pose.position.z = pose.position.z - 0.05
    text_pose.orientation = Quaternion(0, 0, 0, 1)
    text_marker = Marker()
    text_marker.header.frame_id = frame
    text_marker.type = Marker.TEXT_VIEW_FACING
    text_marker.pose = text_pose
    text_marker.scale.z = 0.05
    text_marker.color = color
    text_marker.id = 2 * unique_id + 1
    text_marker.text = f"{label}"
    text_marker.lifetime = rospy.Duration(0)
    return marker, text_marker


def path_from_trajectory(
    joint_trajectory_msg: JointTrajectory,
    move_group: moveit_commander.MoveGroupCommander,
):
    """
    From a MoveIt trajectory message, return a Path for visualizing in RViz.

    Args:
        joint_trajectory_msg (JointTrajectory): JointTrajectory message
        move_group (moveit_commander.MoveGroupCommander): _description_

    Returns:
        path_msg (Path): path message
    """
    path_msg = Path()

    path_msg.header = joint_trajectory_msg.header
    fk_service = rospy.ServiceProxy("compute_fk", GetPositionFK)
    joint_names = move_group.get_active_joints()
    robot_state = move_group.get_current_state()

    for point in joint_trajectory_msg.points:
        joint_state = robot_state.joint_state
        joint_state.name = joint_names
        joint_state.position = point.positions

        fk_response = fk_service(joint_trajectory_msg.header, ["ee_link"], robot_state)
        pose_stamped = fk_response.pose_stamped[0]
        path_msg.poses.append(pose_stamped)

    return path_msg


def matrix_from_pose(pose: Pose) -> np.array:
    """
    Generate 4x4 transformation matrix from a posestamped message.

    Args:
        pose (Pose): PoseStamped input message

    Returns:
        np.array: 4x4 np array
    """
    quaternion = [
        pose.orientation.x,
        pose.orientation.y,
        pose.orientation.z,
        pose.orientation.w,
    ]
    rotation = Rotation.from_quat(quaternion)
    translation = [
        pose.position.x,
        pose.position.y,
        pose.position.z,
    ]
    matrix = np.eye(4)
    matrix[:3, :3] = rotation.as_matrix()
    matrix[:3, 3] = translation
    return matrix


def matrix_from_posestamped(posestamped: PoseStamped) -> np.array:
    """
    Generate 4x4 transformation matrix from a posestamped message.

    Args:
        posestamped (PoseStamped): PoseStamped input message

    Returns:
        np.array: 4x4 np array
    """
    return matrix_from_pose(posestamped.pose)


def jt_traj_from_vel_arr(
    velocities: list,
    positions: list,
    joint_names: list,
    time_from_start: Duration,
    header: Header = None,
) -> JointTrajectory:
    """
    Creates a servo command from a velocity array

    Args:
        vel_array (list): Array of joint velocities
        header (Header, optional): header to add to message. Defaults to None.

    Returns:
        JointTrajectory: JT message
    """
    # Create JointTrajectory message
    trajectory = JointTrajectory()
    trajectory.joint_names = joint_names
    if header is not None:
        trajectory.header = header
    else:
        trajectory.header = Header(frame_id="bravo_base_link")

    #  Create a single JointTrajectoryPoint
    point = JointTrajectoryPoint()
    point.time_from_start = time_from_start

    # Set the velocities
    point.velocities = velocities
    point.positions = positions

    # Add this point to the trajectory
    trajectory.points.append(point)

    return trajectory
