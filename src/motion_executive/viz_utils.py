import numpy as np
from geometry_msgs.msg import Point
from scipy.spatial.transform import Rotation
from std_msgs.msg import ColorRGBA
from visualization_msgs.msg import Marker, MarkerArray


def calculate_manipulability_ellipsoid(jacobian: np.array):
    """
    Calculate ellipsoid from jacobian

    Args:
        jacobian (np.array): input jacobian

    Returns:
        tuple, tuple: _description_
    """
    # Assuming jacobian is a 6xN matrix (N is the number of joints)
    jacobian_pos = jacobian[0:3, :]  # Extract position part of the Jacobian
    jacobian_ori = jacobian[3:6, :]  # Extract orientation part of the Jacobian

    # Compute the manipulability ellipsoid for position (XYZ)
    pos_ellipsoid_matrix = np.dot(jacobian_pos, jacobian_pos.T)
    pos_eigenvalues, pos_eigenvectors = np.linalg.eig(pos_ellipsoid_matrix)

    # Compute the manipulability ellipsoid for orientation (RPY)
    ori_ellipsoid_matrix = np.dot(jacobian_ori, jacobian_ori.T)
    ori_eigenvalues, ori_eigenvectors = np.linalg.eig(ori_ellipsoid_matrix)

    return (pos_eigenvalues, pos_eigenvectors), (ori_eigenvalues, ori_eigenvectors)


def create_ellipsoid_marker(
    eigenvalues, eigenvectors, marker_id, color, frame_id="robot_base_link", scale=0.1
):
    """
    Get ellipsoid marker (single)

    Args:
        eigenvalues (_type_): _description_
        eigenvectors (_type_): _description_
        marker_id (_type_): _description_
        color (_type_): _description_
        frame_id (str, optional): _description_. Defaults to "robot_base_link".
        scale (float, optional): _description_. Defaults to 0.1.

    Returns:
        _type_: _description_
    """
    marker = Marker()
    marker.header.frame_id = frame_id
    marker.type = Marker.SPHERE
    marker.action = Marker.ADD
    marker.id = marker_id

    # Set the scale of the ellipsoid (length of the principal axes)
    marker.scale.x = 2 * np.sqrt(eigenvalues[0]) * scale
    marker.scale.y = 2 * np.sqrt(eigenvalues[1]) * scale
    marker.scale.z = 2 * np.sqrt(eigenvalues[2]) * scale

    (
        marker.pose.orientation.x,
        marker.pose.orientation.y,
        marker.pose.orientation.z,
        marker.pose.orientation.w,
    ) = Rotation.from_matrix(eigenvectors).as_quat()

    # Set the position of the ellipsoid (at the end effector)
    marker.pose.position.x = 0
    marker.pose.position.y = 0
    marker.pose.position.z = 0

    # Set the color of the ellipsoid
    marker.color = color

    return marker


def get_ellisoid_marker_array(
    eigenvalues,
    eigenvectors,
    marker_id,
    color,
    frame_id="robot_base_link",
    scale=0.1,
    draw_axes=False,
):
    """
    Return ellipsoid marker array (multiple)

    Args:
        eigenvalues (_type_): _description_
        eigenvectors (_type_): _description_
        marker_id (_type_): _description_
        color (_type_): _description_
        frame_id (str, optional): _description_. Defaults to "robot_base_link".
        scale (float, optional): _description_. Defaults to 0.1.
        draw_axes (bool, optional): _description_. Defaults to False.

    Returns:
        _type_: _description_
    """
    ellipsoid_array = MarkerArray()
    ellipsoid_marker = create_ellipsoid_marker(
        eigenvalues, eigenvectors, marker_id, color, frame_id, scale
    )

    ellipsoid_array.markers.append(ellipsoid_marker)

    axes_colors = [
        ColorRGBA(1.0, 0.0, 0.0, 0.5),
        ColorRGBA(0.0, 1.0, 0.0, 0.5),
        ColorRGBA(0.0, 0.0, 1.0, 0.5),
    ]
    if draw_axes:
        for i in range(3):
            marker = Marker()
            marker.header.frame_id = frame_id
            marker.type = Marker.ARROW
            marker.action = Marker.ADD
            marker.id = marker_id + i + 1

            # Set the scale of the arrow
            marker.scale.x = 0.025  # Shaft diameter
            marker.scale.y = 0.05  # Head diameter
            marker.scale.z = 0.1  # Head length
            # Set the color of the arrow
            marker.color = axes_colors[i]

            # Set the start and end points of the arrow
            start_point = Point(0, 0, 0)
            end_point = Point(*(eigenvectors[:, i] * np.sqrt(eigenvalues[i])))

            marker.points.append(start_point)
            marker.points.append(end_point)

            ellipsoid_array.markers.append(marker)

    return ellipsoid_array
